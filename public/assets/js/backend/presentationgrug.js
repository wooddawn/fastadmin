define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'Presentationdrug/index',
                    add_url: 'Presentationdrug/add',
                    edit_url: 'Presentationdrug/edit',
                    del_url: 'Presentationdrug/del',
                    multi_url: 'Presentationdrug/multi',
                    table: 'presentation_drug',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_id', title: __('Presentation_id')},
                        {field: 'characterisation_of_drug_role', title: __('Characterisation_of_drug_role')},
                        {field: 'mpid_version_date_number', title: __('Mpid_version_date_number')},
                        {field: 'medicinal_product_identifier_mpid', title: __('Medicinal_product_identifier_mpid')},
                        {field: 'phpid_version_date_number', title: __('Phpid_version_date_number')},
                        {field: 'pharmaceutical_product_identifier_phpid', title: __('Pharmaceutical_product_identifier_phpid')},
                        {field: 'medicinal_product_name_as_reported_by_the_primary_source', title: __('Medicinal_product_name_as_reported_by_the_primary_source')},
                        {field: 'identification_of_the_country_where_the_drug_was_obtained', title: __('Identification_of_the_country_where_the_drug_was_obtained')},
                        {field: 'investigational_product_blinded', title: __('Investigational_product_blinded')},
                        {field: 'authorisation_application_number', title: __('Authorisation_application_number')},
                        {field: 'country_of_authorisation_application', title: __('Country_of_authorisation_application')},
                        {field: 'name_of_holder_applicant', title: __('Name_of_holder_applicant')},
                        {field: 'cumulative_dose_to_first_reaction_number', title: __('Cumulative_dose_to_first_reaction_number')},
                        {field: 'cumulative_dose_to_first_reaction_unit', title: __('Cumulative_dose_to_first_reaction_unit')},
                        {field: 'gestation_period_at_time_of_exposure_number', title: __('Gestation_period_at_time_of_exposure_number')},
                        {field: 'gestation_period_at_time_of_exposure_unit', title: __('Gestation_period_at_time_of_exposure_unit')},
                        {field: 'actions_taken_with_drug', title: __('Actions_taken_with_drug')},
                        {field: 'additional_information_on_drug_free_text', title: __('Additional_information_on_drug_free_text')},
                        {field: 'presentation.name', title: __('Presentation.name')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});