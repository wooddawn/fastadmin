define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentationresult/index',
                    add_url: 'presentationresult/add/pid/'+Config.pid,
                    edit_url: 'presentationresult/edit',
                    del_url: 'presentationresult/del',
                    multi_url: 'presentationresult/multi',
                    table: 'presentationResult',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_id', title: __('Presentation_id')},
                        {field: 'test_date', title: __('Test_date'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'test_name', title: __('Test_name')},
                        {field: 'meddra_version', title: __('Meddra_version')},
                        {field: 'meddra_code', title: __('Meddra_code')},
                        {field: 'code', title: __('Code'), searchList: {"2":__('Code 2'),"3":__('Code 3'),"4":__('Code 4')}, formatter: Table.api.formatter.normal},
                        {field: 'value', title: __('Value')},
                        {field: 'unit', title: __('Unit')},
                        {field: 'normal_low_value', title: __('Normal_low_value')},
                        {field: 'normal_high_value', title: __('Normal_high_value')},
                        {field: 'more_information', title: __('More_information'), searchList: {"1":__('More_information 1'),"0":__('More_information 0')}, formatter: Table.api.formatter.normal},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});