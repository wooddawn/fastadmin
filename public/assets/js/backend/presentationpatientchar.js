define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentationpatientchar/index',
                    add_url: 'presentationpatientchar/add',
                    edit_url: 'presentationpatientchar/edit',
                    del_url: 'presentationpatientchar/del',
                    multi_url: 'presentationpatientchar/multi',
                    table: 'presentation_patient_char',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_id', title: __('Presentation_id')},
                        {field: 'patient_name_or_initials', title: __('Patient_name_or_initials')},
                        {field: 'gp_medical_record_number', title: __('Gp_medical_record_number')},
                        {field: 'specialist_record_number', title: __('Specialist_record_number')},
                        {field: 'hospital_record_number', title: __('Hospital_record_number')},
                        {field: 'investigation_number', title: __('Investigation_number')},
                        {field: 'body_weight_kg', title: __('Body_weight_kg')},
                        {field: 'height_cm', title: __('Height_cm')},
                        {
                            field: 'sex',
                            title: __('Sex'),
                            searchList: {"1": __('Sex 1'), "2": __('Sex 2')},
                            formatter: Table.api.formatter.normal
                        },
                        {field: 'last_menstrual_period_date', title: __('Last_menstrual_period_date')},
                        {field: 'reaction_event', title: __('Reaction_event')},
                        {
                            field: 'concomitant_therapies',
                            title: __('Concomitant_therapies'),
                            searchList: {"1": __('Concomitant_therapies 1'), "0": __('Concomitant_therapies 0')},
                            formatter: Table.api.formatter.normal
                        },
                        {field: 'date_of_death', title: __('Date_of_death')},
                        {
                            field: 'was_autopsy_done',
                            title: __('Was_autopsy_done'),
                            searchList: {"1": __('Was_autopsy_done 1'), "0": __('Was_autopsy_done 0')},
                            formatter: Table.api.formatter.normal
                        },
                        {field: 'presentation.name', title: __('Presentation.name')},
                        {
                            field: 'operate',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });


            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Table.api.init({
                extend: {
                    index_url: '',
                    add_url: '',
                    edit_url: '',
                    del_url: '',
                    multi_url: '',
                    table: '',
                }
            });
            Controller.api.bindevent();
            Controller.api.showtable2();
            Controller.api.showtable3();
            Controller.api.showtable4();
            Controller.api.showtable5();
            Controller.api.showtable6();
            Controller.api.showtable7();
            Controller.api.showtable8();
            Controller.api.showtable9();

            $(document).on("click", ".infomaition", function (t) {
                var that = this;
                var numbers = $(that).data('number');
                //'Controller.api.showtable'+;
                Fast.api.open(
                    $(that).data('href'),
                    $(this).html()
                );
            });
        },
        api: {

            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            showtable2:function(){
                Table.api.init({
                    extend: {
                        index_url: '',
                        add_url: '',
                        edit_url: '',
                        del_url: '',
                        multi_url: '',
                        table: '',
                    }
                });
                var table2 = $("#table2");
                // 初始化表格
                table2.bootstrapTable({
                    url: '/admin/presentationpatientcharageinfomation/index?sp_key=presentation_patient_char_id&sp_id='+Config.pid,
                    pk: 'id',
                    toolbar:'#toolbar2',
                    sortName: 'id',
                    columns: [
                        [
                            {field: 'id', title: __('Id')},
                            {field: 'date_of_birth', title: __('Date_of_birth')},
                            {field: 'age_at_time_of_onset_of_reaction_event_number', title: __('Age_at_time_of_onset_of_reaction_event_number')},
                        ]
                    ]
                });
                // 为表格绑定事件
                Table.api.bindevent(table2);
            },
            showtable3:function(){
                Table.api.init({
                    extend: {
                        index_url: '',
                        add_url: '',
                        edit_url: '',
                        del_url: '',
                        multi_url: '',
                        table: '',
                    }
                });
                var table3 = $("#table3");
                // 初始化表格
                //op={"presentation_patient_char_id":"="}&filter={"presentation_patient_char_id":'+Config.pid+'}
                table3.bootstrapTable({
                    url: '/admin/presentationpatientcharmedicalistory/index?sp_key=presentation_patient_char_id&sp_id='+Config.pid,
                    pk: 'id',
                    sortName: 'id',
                    toolbar:'#toolbar3',
                    columns: [
                        [
                            {field: 'id', title: __('Id')},
                            {field: 'presentation_patient_char_id', title: __('Presentation_patient_char_id')},
                            {field: 'meddra_version_for_medical_history', title: __('Meddra_version_for_medical_history')},
                            {field: 'medical_history_disease_surgical_procedure_etc_meddra_code', title: __('Medical_history_disease_surgical_procedure_etc_meddra_code')},
                            {field: 'start_date', title: __('Start_date')},
                            {field: 'continuing', title: __('Continuing')},
                            {field: 'end_date', title: __('End_date')},
                            {field: 'family_historyd', title: __('Family_historyd')},
                            {field: 'char.patient_name_or_initials', title: __('Char.patient_name_or_initials')},
                        ]
                    ]
                });
                // 为表格绑定事件
                Table.api.bindevent(table3);
            },
            showtable4:function(){
                Table.api.init({
                    extend: {
                        index_url: '',
                        add_url: '',
                        edit_url: '',
                        del_url: '',
                        multi_url: '',
                        table: '',
                    }
                });
                var table4 = $("#table4");
                // 初始化表格
                table4.bootstrapTable({
                    url: '/admin/presentationpatientchardrughistory/index?sp_key=presentation_patient_char_id&sp_id='+Config.pid,
                    pk: 'id',
                    sortName: 'id',
                    toolbar:'#toolbar4',
                    columns: [
                        [
                            {field: 'id', title: __('Id')},
                            {field: 'presentation_patient_char_id', title: __('Presentation_patient_char_id')},
                            {field: 'name_of_drug_as_reported', title: __('Name_of_drug_as_reported')},
                            {field: 'mpid_version_date_number', title: __('Mpid_version_date_number')},
                            {field: 'medicinal_product_identifier_mpid', title: __('Medicinal_product_identifier_mpid')},
                            {field: 'phpid_version_date_number', title: __('Phpid_version_date_number')},
                            {field: 'pharmaceutical_product_identifier_phpid', title: __('Pharmaceutical_product_identifier_phpid')},
                            {field: 'start_date', title: __('Start_date')},
                            {field: 'end_date', title: __('End_date')},
                            {field: 'meddra_version_for_indication', title: __('Meddra_version_for_indication')},
                            {field: 'indication_meddra_code', title: __('Indication_meddra_code')},
                            {field: 'meddra_version_for_reaction', title: __('Meddra_version_for_reaction')},
                            {field: 'reaction_meddra_code', title: __('Reaction_meddra_code')},
                            {field: 'char.patient_name_or_initials', title: __('Char.patient_name_or_initials')},
                            {field: 'operate', title: __('Operate'), table: table4, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                        ]
                    ]
                });
                // 为表格绑定事件
                Table.api.bindevent(table4);
            },
            showtable5:function(){
                Table.api.init({
                    extend: {
                        index_url: '',
                        add_url: '',
                        edit_url: '',
                        del_url: '',
                        multi_url: '',
                        table: '',
                    }
                });
                var table5 = $("#table5");
                // 初始化表格
                table5.bootstrapTable({
                    url: '/admin/presentationpatientcharreporteddeath/index?sp_key=presentation_patient_char_id&sp_id='+Config.pid,
                    pk: 'id',
                    sortName: 'id',
                    toolbar:'#toolbar5',
                    columns: [
                        [
                            {field: 'id', title: __('Id')},
                            {field: 'presentation_patient_char_id', title: __('Presentation_patient_char_id')},
                            {field: 'meddra_version_for_reported_causes_of_death', title: __('Meddra_version_for_reported_causes_of_death')},
                            {field: 'reported_causes_of_death_meddra_code', title: __('Reported_causes_of_death_meddra_code')},
                            {field: 'reported_causes_of_death_free_text', title: __('Reported_causes_of_death_free_text')},
                            {field: 'char.patient_name_or_initials', title: __('Char.patient_name_or_initials')},
                        ]
                    ]
                });
                // 为表格绑定事件
                Table.api.bindevent(table5);
            },
            showtable6:function(){
                Table.api.init({
                    extend: {
                        index_url: '',
                        add_url: '',
                        edit_url: '',
                        del_url: '',
                        multi_url: '',
                        table: '',
                    }
                });
                var table6 = $("#table6");
                // 初始化表格
                table6.bootstrapTable({
                    url: '/admin/presentationpatientchardetermineddeath/index?sp_key=presentation_patient_char_id&sp_id='+Config.pid,
                    pk: 'id',
                    sortName: 'id',
                    toolbar:'#toolbar6',
                    columns: [
                        [
                            {field: 'id', title: __('Id')},
                            {field: 'presentation_patient_char_id', title: __('Presentation_patient_char_id')},
                            {field: 'meddra_version_for_autopsy_determined_causes_of_death', title: __('Meddra_version_for_autopsy_determined_causes_of_death')},
                            {field: 'autopsy_determined_causes_of_death_meddra_code', title: __('Autopsy-determined_causes_of_death_meddra_code')},
                            {field: 'autopsy_determined_causes_of_death_free_text', title: __('Autopsy-determined_causes_of_death_free_text')},
                            {field: 'char.patient_name_or_initials', title: __('Char.patient_name_or_initials')},
                        ]
                    ]
                });
                // 为表格绑定事件
                Table.api.bindevent(table6);
            },
            showtable7:function(){
                Table.api.init({
                    extend: {
                        index_url: '',
                        add_url: '',
                        edit_url: '',
                        del_url: '',
                        multi_url: '',
                        table: '',
                    }
                });
                var table7 = $("#table7");
                // 初始化表格
                table7.bootstrapTable({
                    url: '/admin/presentationpatientcharparentinformation/index?sp_key=presentation_patient_char_id&sp_id='+Config.pid,
                    pk: 'id',
                    sortName: 'id',
                    toolbar:'#toolbar7',
                    columns: [
                        [
                            {field: 'id', title: __('Id')},
                            {field: 'presentation_patient_char_id', title: __('Presentation_patient_char_id')},
                            {field: 'parent_identification', title: __('Parent_identification')},
                            {field: 'date_of_birth_of_parent', title: __('Date_of_birth_of_parent')},
                            {field: 'age_of_parent', title: __('Age_of_parent')},
                            {field: 'age_of_parent_number', title: __('Age_of_parent_number')},
                            {field: 'age_of_parent_unit', title: __('Age_of_parent_unit')},
                            {field: 'last_menstrual_period_date', title: __('Last_menstrual_period_date')},
                            {field: 'body_weight_kg_of_parent', title: __('Body_weight_kg_of_parent')},
                            {field: 'height_cm_of_parent', title: __('Height_cm_of_parent')},
                            {field: 'sex_of_parentd', title: __('Sex_of_parentd'), searchList: {"1":__('Sex_of_parentd 1'),"2":__('Sex_of_parentd 2')}, formatter: Table.api.formatter.normal},
                        ]
                    ]
                });
                // 为表格绑定事件
                Table.api.bindevent(table7);
            },
            showtable8:function(){
                Table.api.init({
                    extend: {
                        index_url: '',
                        add_url: '',
                        edit_url: '',
                        del_url: '',
                        multi_url: '',
                        table: '',
                    }
                });
                var table8 = $("#table8");
                // 初始化表格
                table8.bootstrapTable({
                    url: '/admin/presentationpatientcharparentconditions/index?sp_key=presentation_patient_char_id&sp_id='+Config.pid,
                    pk: 'id',
                    sortName: 'id',
                    toolbar:'#toolbar8',
                    columns: [
                        [
                            {field: 'id', title: __('Id')},
                            {field: 'presentation_patient_char_id', title: __('Presentation_patient_char_id')},
                            {field: 'char.patient_name_or_initials', title: __('Char.patient_name_or_initials')},
                        ]
                    ]
                });
                // 为表格绑定事件
                Table.api.bindevent(table8);
            },
            showtable9:function(){
                Table.api.init({
                    extend: {
                        index_url: '',
                        add_url: '',
                        edit_url: '',
                        del_url: '',
                        multi_url: '',
                        table: '',
                    }
                });
                var table9 = $("#table9");
                // 初始化表格
                table9.bootstrapTable({
                    url: '/admin/presentationpatientcharparentdrughistory/index?sp_key=presentation_patient_char_id&sp_id='+Config.pid,
                    pk: 'id',
                    sortName: 'id',
                    toolbar:'#toolbar9',
                    columns: [
                        [
                            {field: 'id', title: __('Id')},
                            {field: 'presentation_patient_char_id', title: __('Presentation_patient_char_id')},
                            {field: 'name_of_drug_as_reported', title: __('Name_of_drug_as_reported')},
                            {field: 'mpid_version_date_number', title: __('Mpid_version_date_number')},
                            {field: 'medicinal_product_identifier_mpid', title: __('Medicinal_product_identifier_mpid')},
                            {field: 'phpid_version_date_number', title: __('Phpid_version_date_number')},
                            {field: 'pharmaceutical_product_identifier_phpid_', title: __('Pharmaceutical_product_identifier_phpid_')},
                            {field: 'start_date', title: __('Start_date')},
                            {field: 'end_date', title: __('End_date')},
                            {field: 'meddra_version_for_indication', title: __('Meddra_version_for_indication')},
                            {field: 'indication_meddra_code', title: __('Indication_meddra_code')},
                            {field: 'meddra_version_for_reaction', title: __('Meddra_version_for_reaction')},
                            {field: 'reactions_meddra_code', title: __('Reactions_meddra_code')},
                            {field: 'char.patient_name_or_initials', title: __('Char.patient_name_or_initials')},
                        ]
                    ]
                });
                // 为表格绑定事件
                Table.api.bindevent(table9);
            },
        },
    };
    return Controller;
});