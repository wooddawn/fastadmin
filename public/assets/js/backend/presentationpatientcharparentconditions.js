define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentationpatientcharparentconditions/index',
                    add_url: 'presentationpatientcharparentconditions/add',
                    edit_url: 'presentationpatientcharparentconditions/edit',
                    del_url: 'presentationpatientcharparentconditions/del',
                    multi_url: 'presentationpatientcharparentconditions/multi',
                    table: 'presentation_patient_char_parent_conditions',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_patient_char_id', title: __('Presentation_patient_char_id')},
                        {field: 'char.patient_name_or_initials', title: __('Char.patient_name_or_initials')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});