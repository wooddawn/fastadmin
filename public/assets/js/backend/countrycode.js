define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'countrycode/index',
                    add_url: 'countrycode/add',
                    edit_url: 'countrycode/edit',
                    del_url: 'countrycode/del',
                    multi_url: 'countrycode/multi',
                    table: 'countrycode',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name_cn', title: __('Name_cn')},
                        {field: 'name_en', title: __('Name_en')},
                        {field: 'name_fn', title: __('Name_fn')},
                        {field: 'alpha_2', title: __('Alpha_2')},
                        {field: 'alpha_3', title: __('Alpha_3')},
                        {field: 'numberic', title: __('Numberic')},
                        {field: 'remark', title: __('Remark')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});