define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentationinformation/index',
                    add_url: 'presentationinformation/add',
                    edit_url: 'presentationinformation/edit',
                    del_url: 'presentationinformation/del',
                    multi_url: 'presentationinformation/multi',
                    table: 'presentationInformation',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'fa_presentation_id', title: __('Fa_presentation_id')},
                        {field: 'title', title: __('Title')},
                        {field: 'givenname', title: __('Givenname')},
                        {field: 'middlename', title: __('Middlename')},
                        {field: 'familyname', title: __('Familyname')},
                        {field: 'organisation', title: __('Organisation')},
                        {field: 'department', title: __('Department')},
                        {field: 'address', title: __('Address')},
                        {field: 'city', title: __('City')},
                        {field: 'province', title: __('Province')},
                        {field: 'postcode', title: __('Postcode')},
                        {field: 'telephone', title: __('Telephone')},
                        {field: 'countycode', title: __('Countycode')},
                        {field: 'qualification', title: __('Qualification'), searchList: {"1":__('Qualification 1'),"2":__('Qualification 2'),"3":__('Qualification 3'),"4":__('Qualification 4'),"5":__('Qualification 5')}, formatter: Table.api.formatter.normal},
                        {field: 'type', title: __('Type'), searchList: {"1":__('Type 1'),"2":__('Type 2'),"3":__('Type 3'),"4":__('Type 4'),"5":__('Type 5'),"6 ":__('Type 6 '),"7":__('Type 7')}, formatter: Table.api.formatter.normal},
                        {field: 'fax', title: __('Fax')},
                        {field: 'email', title: __('Email')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});