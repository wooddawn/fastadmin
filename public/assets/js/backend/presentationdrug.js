define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentationdrug/index',
                    add_url: 'presentationdrug/add',
                    edit_url: 'presentationdrug/edit',
                    del_url: 'presentationdrug/del',
                    multi_url: 'presentationdrug/multi',
                    table: 'presentation_drug',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_id', title: __('Presentation_id')},
                        {field: 'characterisation_of_drug_role', title: __('Characterisation_of_drug_role'), searchList: {"1 ":__('Characterisation_of_drug_role 1 '),"2 ":__('Characterisation_of_drug_role 2 '),"3 ":__('Characterisation_of_drug_role 3 '),"4 ":__('Characterisation_of_drug_role 4 ')}, formatter: Table.api.formatter.normal},
                        {field: 'mpid_version_date_number', title: __('Mpid_version_date_number')},
                        {field: 'medicinal_product_identifier_mpid', title: __('Medicinal_product_identifier_mpid')},
                        {field: 'phpid_version_date_number', title: __('Phpid_version_date_number')},
                        {field: 'pharmaceutical_product_identifier_phpid', title: __('Pharmaceutical_product_identifier_phpid')},
                        {field: 'medicinal_product_name_as_reported_by_the_primary_source', title: __('Medicinal_product_name_as_reported_by_the_primary_source')},
                        {field: 'identification_of_the_country_where_the_drug_was_obtained', title: __('Identification_of_the_country_where_the_drug_was_obtained')},
                        {field: 'investigational_product_blinded', title: __('Investigational_product_blinded')},
                        {field: 'authorisation_application_number', title: __('Authorisation_application_number')},
                        {field: 'country_of_authorisation_application', title: __('Country_of_authorisation_application')},
                        {field: 'name_of_holder_applicant', title: __('Name_of_holder_applicant')},
                        {field: 'cumulative_dose_to_first_reaction_number', title: __('Cumulative_dose_to_first_reaction_number')},
                        {field: 'cumulative_dose_to_first_reaction_unit', title: __('Cumulative_dose_to_first_reaction_unit')},
                        {field: 'gestation_period_at_time_of_exposure_number', title: __('Gestation_period_at_time_of_exposure_number')},
                        {field: 'gestation_period_at_time_of_exposure_unit', title: __('Gestation_period_at_time_of_exposure_unit')},
                        {field: 'actions_taken_with_drug', title: __('Actions_taken_with_drug'), searchList: {"1":__('Actions_taken_with_drug 1'),"2":__('Actions_taken_with_drug 2'),"3":__('Actions_taken_with_drug 3'),"4":__('Actions_taken_with_drug 4'),"0":__('Actions_taken_with_drug 0'),"9":__('Actions_taken_with_drug 9')}, formatter: Table.api.formatter.normal},
                        {field: 'presentation.name', title: __('Presentation.name')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
            Controller.api.showtable2();
            Controller.api.showtable3();
            Controller.api.showtable4();
            Controller.api.showtable5();
            //Controller.api.showtable6();
            Controller.api.showtable7();

            $(document).on("click", ".infomaition", function (t) {
                var that = this;
                var numbers = $(that).data('number');
                Fast.api.open(
                    $(that).data('href'),
                    $(this).html(),
                    {
                        end: function (data) {
                        }
                    }
                );
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            showtable2:function(){
                Table.api.init({
                    extend: {
                        index_url: '',
                        add_url: '',
                        edit_url: '',
                        del_url: '',
                        multi_url: '',
                        table: '',
                    }
                });
                var table2 = $("#table2");
                // 初始化表格
                table2.bootstrapTable({
                    url: '/admin/presentationdrugcase/index?sp_key=presentation_drug_id&sp_id='+Config.pid,
                    pk: 'id',
                    toolbar:'#toolbar2',
                    sortName: 'id',
                    columns: [
                        [
                            {field: 'id', title: __('Id')},
                            {field: 'indication_as_reported_by_the_primary_source', title: __('Indication_as_reported_by_the_primary_source')},
                            {field: 'meddra_version_for_indication', title: __('Meddra_version_for_indication')},
                            {field: 'indication_meddra_code', title: __('Indication_meddra_code')}
                        ]
                    ]
                });
                // 为表格绑定事件
                Table.api.bindevent(table2);
            },
            showtable3:function(){
                Table.api.init({
                    extend: {
                        index_url: '',
                        add_url: '',
                        edit_url: '',
                        del_url: '',
                        multi_url: '',
                        table: '',
                    }
                });
                var table3 = $("#table3");
                // 初始化表格
                table3.bootstrapTable({
                    url: '/admin/presentationdrugdosage/index?sp_key=presentation_drug_id&sp_id='+Config.pid,
                    pk: 'id',
                    toolbar:'#toolbar3',
                    sortName: 'id',
                    columns: [
                        [
                            {field: 'id', title: __('Id')},
                            {field: 'presentation_drug_id', title: __('Presentation_drug_id')},
                            {field: 'dose_number', title: __('Dose_number')},
                            {field: 'dose_unit', title: __('Dose_unit')},
                            {field: 'number_of_units_in_the_interval', title: __('Number_of_units_in_the_interval')},
                            {field: 'definition_of_the_time_interval_unit', title: __('Definition_of_the_time_interval_unit')},
                            {field: 'date_and_time_of_start_of_drug', title: __('Date_and_time_of_start_of_drug')},
                            {field: 'date_and_time_of_last_administration', title: __('Date_and_time_of_last_administration')},
                            {field: 'duration_of_drug_administration_number', title: __('Duration_of_drug_administration_number')},
                            {field: 'duration_of_drug_administration_unit', title: __('Duration_of_drug_administration_unit')},
                            {field: 'batch_lot_number', title: __('Batch_lot_number')},
                            {field: 'pharmaceutical_dose_form_free_text', title: __('Pharmaceutical_dose_form_free_text')},
                            {field: 'pharmaceutical_dose_form_termid_version_date_number', title: __('Pharmaceutical_dose_form_termid_version_date_number')},
                            {field: 'pharmaceutical_dose_form_termid', title: __('Pharmaceutical_dose_form_termid')},
                            {field: 'route_of_administration_free_text', title: __('Route_of_administration_free_text')},
                            {field: 'route_of_administration_termid_version_date_number', title: __('Route_of_administration_termid_version_date_number')},
                            {field: 'route_of_administration_termid', title: __('Route_of_administration_termid')},
                            {field: 'parent_route_of_administration_free_text', title: __('Parent_route_of_administration_free_text')},
                            {field: 'parent_route_of_administration_termid_version_date_number', title: __('Parent_route_of_administration_termid_version_date_number')},
                            {field: 'parent_route_of_administration_termidg', title: __('Parent_route_of_administration_termidg')},
                            {field: 'drug.characterisation_of_drug_role', title: __('Drug.characterisation_of_drug_role')}
                        ]
                    ]
                });
                // 为表格绑定事件
                Table.api.bindevent(table3);
            },
            showtable4:function(){
                Table.api.init({
                    extend: {
                        index_url: '',
                        add_url: '',
                        edit_url: '',
                        del_url: '',
                        multi_url: '',
                        table: '',
                    }
                });
                var table4 = $("#table4");
                // 初始化表格
                table4.bootstrapTable({
                    url: '/admin/presentationdrugidentifier/index?sp_key=presentation_drug_id&sp_id='+Config.pid,
                    pk: 'id',
                    toolbar:'#toolbar4',
                    sortName: 'id',
                    columns: [
                        [
                            {field: 'id', title: __('Id')},
                            {field: 'presentation_drug_id', title: __('Presentation_drug_id')},
                            {field: 'substance_specified_substance_name', title: __('Substance_specified_substance_name')},
                            {field: 'substancespecified_substance_termid_version_date_number', title: __('Substancespecified_substance_termid_version_date_number')},
                            {field: 'substancespecified_substance_termid', title: __('Substancespecified_substance_termid')},
                            {field: 'strength_number', title: __('Strength_number')},
                            {field: 'strength_unit', title: __('Strength_unit')},
                            {field: 'drug.characterisation_of_drug_role', title: __('Drug.characterisation_of_drug_role')}
                        ]
                    ]
                });
                // 为表格绑定事件
                Table.api.bindevent(table4);
            },
            showtable5:function(){
                Table.api.init({
                    extend: {
                        index_url: '',
                        add_url: '',
                        edit_url: '',
                        del_url: '',
                        multi_url: '',
                        table: '',
                    }
                });
                var table5 = $("#table5");
                // 初始化表格
                table5.bootstrapTable({
                    url: '/admin/presentationdrugmatrix/index?sp_key=presentation_drug_id&sp_id='+Config.pid,
                    pk: 'id',
                    toolbar:'#toolbar5',
                    sortName: 'id',
                    columns: [
                        [
                            {field: 'id', title: __('Id')},
                            {field: 'presentation_drug_id', title: __('Presentation_drug_id')},
                            {field: 'reactions_events_assessed', title: __('Reactions_events_assessed')},
                            {field: 'administration_and_start_of_reaction_event_number', title: __('Administration_and_start_of_reaction_event_number')},
                            {field: 'administration_and_start_of_reaction_event_unit', title: __('Administration_and_start_of_reaction_event_unit')},
                            {field: 'reaction_event_number', title: __('Reaction_event_number')},
                            {field: 'reaction_event_unit', title: __('Reaction_event_unit')},
                            {field: 'did_reaction_recur_on_re_administration', title: __('Did_reaction_recur_on_re_administration'), searchList: {"1 ":__('Did_reaction_recur_on_re_administration 1 '),"2 ":__('Did_reaction_recur_on_re_administration 2 '),"3 ":__('Did_reaction_recur_on_re_administration 3 '),"4 ":__('Did_reaction_recur_on_re_administration 4 ')}, formatter: Table.api.formatter.normal},
                            {field: 'drug.characterisation_of_drug_role', title: __('Drug.characterisation_of_drug_role')},
                        ]
                    ]
                });
                // 为表格绑定事件
                Table.api.bindevent(table5);
            },
            showtable6:function(){
                Table.api.init({
                    extend: {
                        index_url: '',
                        add_url: '',
                        edit_url: '',
                        del_url: '',
                        multi_url: '',
                        table: '',
                    }
                });
                var table6 = $("#table6");
                // 初始化表格
                table6.bootstrapTable({
                    url: '/admin/presentationdrugmatrixassessment/index?sp_key=presentation_drug_id&sp_id='+Config.pid,
                    pk: 'id',
                    toolbar:'#toolbar6',
                    sortName: 'id',
                    columns: [
                        [
                            {field: 'id', title: __('Id')},
                            {field: 'presentation_drug_matrix_id', title: __('Presentation_drug_matrix_id')},
                            {field: 'source_of_assessment', title: __('Source_of_assessment')},
                            {field: 'method_of_assessment', title: __('Method_of_assessment')},
                            {field: 'result_of_assessmentg', title: __('Result_of_assessmentg')},
                            {field: 'matrix.reactions_events_assessed', title: __('Matrix.reactions_events_assessed')},
                        ]
                    ]
                });
                // 为表格绑定事件
                Table.api.bindevent(table6);
            },
            showtable7:function(){
                Table.api.init({
                    extend: {
                        index_url: '',
                        add_url: '',
                        edit_url: '',
                        del_url: '',
                        multi_url: '',
                        table: '',
                    }
                });
                var table7 = $("#table7");
                // 初始化表格
                table7.bootstrapTable({
                    url: '/admin/presentationdrugother/index?sp_key=presentation_drug_id&sp_id='+Config.pid,
                    pk: 'id',
                    toolbar:'#toolbar7',
                    sortName: 'id',
                    columns: [
                        [
                            {field: 'id', title: __('Id')},
                            {field: 'presentation_drug_id', title: __('Presentation_drug_id')},
                            {field: 'additional_information_on_drug_coded', title: __('Additional_information_on_drug_coded'), searchList: {"1":__('Additional_information_on_drug_coded 1'),"2":__('Additional_information_on_drug_coded 2'),"3":__('Additional_information_on_drug_coded 3'),"4":__('Additional_information_on_drug_coded 4'),"5":__('Additional_information_on_drug_coded 5'),"6":__('Additional_information_on_drug_coded 6'),"7":__('Additional_information_on_drug_coded 7'),"8":__('Additional_information_on_drug_coded 8'),"9":__('Additional_information_on_drug_coded 9'),"10":__('Additional_information_on_drug_coded 10'),"11":__('Additional_information_on_drug_coded 11')}, formatter: Table.api.formatter.normal},
                            {field: 'drug.characterisation_of_drug_role', title: __('Drug.characterisation_of_drug_role')},
                        ]
                    ]
                });
                // 为表格绑定事件
                Table.api.bindevent(table7);
            },
        }
    };
    return Controller;
});