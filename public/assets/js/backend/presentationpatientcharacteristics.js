define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentationpatientcharacteristics/index',
                    add_url: 'presentationpatientcharacteristics/add',
                    edit_url: 'presentationpatientcharacteristics/edit',
                    del_url: 'presentationpatientcharacteristics/del',
                    multi_url: 'presentationpatientcharacteristics/multi',
                    table: 'presentationPatientCharacteristics',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_id', title: __('Presentation_id')},
                        {field: 'patient_name', title: __('Patient_name')},
                        {field: 'gp', title: __('Gp')},
                        {field: 'sr', title: __('Sr')},
                        {field: 'hr', title: __('Hr')},
                        {field: 'investigation', title: __('Investigation')},
                        {field: 'birthday', title: __('Birthday'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'age_number', title: __('Age_number')},
                        {field: 'age_unit', title: __('Age_unit')},
                        {field: 'age_number_baby', title: __('Age_number_baby')},
                        {field: 'age_unit_baby', title: __('Age_unit_baby')},
                        {field: 'age_group', title: __('Age_group'), searchList: {"0":__('Age_group 0'),"1":__('Age_group 1'),"2":__('Age_group 2'),"3":__('Age_group 3'),"4":__('Age_group 4'),"5":__('Age_group 5'),"6":__('Age_group 6')}, formatter: Table.api.formatter.normal},
                        {field: 'body_weight', title: __('Body_weight')},
                        {field: 'height', title: __('Height')},
                        {field: 'sex', title: __('Sex'), searchList: {"1":__('Sex 1'),"2":__('Sex 2')}, formatter: Table.api.formatter.normal},
                        {field: 'last_menstrual_period_date', title: __('Last_menstrual_period_date'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'structured_ids', title: __('Structured_ids')},
                        {field: 'concomitant_therapies', title: __('Concomitant_therapies')},
                        {field: 'past_drug_history_ids', title: __('Past_drug_history_ids')},
                        {field: 'death_ids', title: __('Death_ids')},
                        {field: 'parent_info_ids', title: __('Parent_info_ids')},
                        {field: 'parent_identification', title: __('Parent_identification')},
                        {field: 'parent_birthday', title: __('Parent_birthday'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'parent_age_number', title: __('Parent_age_number')},
                        {field: 'parent_age_unit', title: __('Parent_age_unit')},
                        {field: 'parent_last_menstrual_period_date', title: __('Parent_last_menstrual_period_date')},
                        {field: 'parent_weight', title: __('Parent_weight')},
                        {field: 'parent_height', title: __('Parent_height')},
                        {field: 'parent_sex', title: __('Parent_sex'), searchList: {"1":__('Parent_sex 1'),"2":__('Parent_sex 2')}, formatter: Table.api.formatter.normal},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});