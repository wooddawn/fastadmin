define(['jquery', 'bootstrap', 'backend', 'table', 'form','selectpage'], function ($, undefined, Backend, Table, Form,selectpage) {

    var Controller = {
        check0: function () {
            Controller.index(1);
        },
        check1: function () {
            Controller.index(2);
        },
        check2: function () {
            Controller.index(3);
        },
        check3: function () {
            Controller.index(4);
        },
        check4: function () {
            Controller.index(5);
        },
        check5: function () {
            Controller.index(6);
        },
        index: function (type) {
            type = type ? type:0;
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentation/index?type='+type,
                    add_url: 'presentation/add',
                    edit_url: 'presentation/edit',
                    del_url: 'presentation/del',
                    multi_url: 'presentation/multi',
                    table: 'presentation',
                }
            });

            var table = $("#table");
            var _listButtons = {
                    text: '查看报告',
                    name: 'detail',
                    title: __('View Presentation'),
                    classname: 'btn btn-xs btn-primary btn-dialog',
                    icon: 'fa fa-push',
                    extend: 'data-area=\'["600px","400px"]\'',
                    url: 'presentation/detail',
                    callback: function (data) {
                    }
                },
                _getBottuns = {
                    text: '领取报告',
                    name: 'detail',
                    title: __('Get Presentation'),
                    classname: 'btn btn-xs btn-primary btn-ajax',
                    icon: '',
                    url: 'presentation/check/status/1',
                    callback: function (data) {
                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                    }
                };
            var _buttons = [];
            _buttons = [_listButtons];
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'factory.name', title: __('Factory.name')},
                        {field: 'medicine.name', title: __('Medicine.name')},
                        // {field: 'pharmaceutical_factory_id', title: __('Pharmaceutical_factory_id')},
                        // {field: 'medicine_id', title: __('Medicine_id')},
                        {field: 'argno', title: __('Argno')},
                        {field: 'name', title: __('Name')},
                        {
                            field: 'market',
                            title: __('Market'),
                            searchList: {"0": __('Market 0'), "1": __('Market 1')},
                            formatter: Table.api.formatter.normal
                        },
                        {field: 'date', title: __('Date'), operate: 'RANGE', addclass: 'datetimerange'},
                        {
                            field: 'isindex',
                            title: __('Isindex'),
                            searchList: {"0": __('Isindex 0'), "1": __('Isindex 1')},
                            formatter: Table.api.formatter.normal
                        },
                        {
                            field: 'status',
                            title: __('Status'),
                            searchList: {
                                "0": __('Status 0'),
                                "1": __('Status 1'),
                                "2": __('Status 2'),
                                "3": __('Status 3'),
                                "4": __('Status 4'),
                                "5": __('Status 5'),
                                "6": __('Status 6')
                            },
                            formatter: Table.api.formatter.normal
                        },
                        {field: 'doctor', title: __('Doctor')},
                        // {field: 'pdffiles', title: __('Pdffiles')},
                        {
                            field: 'createtime',
                            title: __('Createtime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'updatetime',
                            title: __('Updatetime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate', title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            buttons: _buttons,
                            formatter: function(value, row, index){
                                var that = $.extend({}, this);
                                var table = $(that.table).clone(true);
                                $(table).data("operate-del", null);
                                $(table).data("operate-edit", null);
                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }
                        }
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        create: function () {
            // 初始化表格参数配置
            Table.api.init();

            //绑定事件
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var panel = $($(this).attr("href"));
                if (panel.size() > 0) {
                    Controller.table[panel.attr("id")].call(this);
                    $(this).on('click', function (e) {
                        $($(this).attr("href")).find(".btn-refresh").trigger("click");
                    });
                }
                //移除绑定的事件
                $(this).unbind('shown.bs.tab');
            });

            //必须默认触发shown.bs.tab事件
            $('ul.nav-tabs li.active a[data-toggle="tab"]').trigger("shown.bs.tab");
        },
        table: {
            basic_c_1: function (pids) {
            },
            basic_c_2: function (pids) {
                // 表格2
                var table2 = $("#table2");
                table2.bootstrapTable({
                    url: 'presentationsource/index',
                    extend: {
                        index_url: '',
                        add_url: 'presentationsource/add',
                        edit_url: 'presentationsource/edit',
                        del_url: 'presentationsource/del',
                        multi_url: '',
                        table: '',
                    },
                    toolbar: '#toolbar2',
                    sortName: 'id',
                    search: false,
                    columns: [
                        [
                            {checkbox: true},
                            {field: 'id', title: __('Id')},
                            {field: 'presentation_id', title: __('Presentation_id')},
                            {field: 'title', title: __('Title')},
                            {field: 'givenname', title: __('Givenname')},
                            {field: 'middlename', title: __('Middlename')},
                            {field: 'familyname', title: __('Familyname')},
                            {field: 'organisation', title: __('Organisation')},
                            {field: 'department', title: __('Department')},
                            {field: 'address', title: __('Address')},
                            {field: 'city', title: __('City')},
                            {field: 'province', title: __('Province')},
                            {field: 'postcode', title: __('Postcode')},
                            {field: 'telephone', title: __('Telephone')},
                            {field: 'countycode', title: __('Countycode')},
                            {
                                field: 'qualification',
                                title: __('Qualification'),
                                searchList: {
                                    "1": __('Qualification 1'),
                                    "2": __('Qualification 2'),
                                    "3": __('Qualification 3'),
                                    "4": __('Qualification 4'),
                                    "5": __('Qualification 5')
                                },
                                formatter: Table.api.formatter.normal
                            },
                            {field: 'source', title: __('Source')},
                            {
                                field: 'operate',
                                title: __('Operate'),
                                table: table2,
                                events: Table.api.events.operate,
                                formatter: Table.api.formatter.operate
                            }
                        ]
                    ]
                });

                // 为表格2绑定事件
                Table.api.bindevent(table2);
            },
            basic_c_3: function (pids) {
            },
            basic_c_4: function (pids) {
            },
            basic_c_5: function (pids) {
            },
            basic_c_6: function (pids) {
            },
            basic_c_7: function (pids) {
            },
            basic_c_8: function (pids) {
            },
            basic_c_9: function (pids) {
            },
            basic_c_10: function (pids) {
            },
            api: {
                bindevent: function () {
                    Form.api.bindevent($("form[role=form]"));
                }
            }
        },
        add: function () {
            Controller.api.bindevent();
            Controller.ext();
        },
        edit: function () {
            Controller.api.bindevent();
            Controller.ext();
        },
        detail:function(){
            $(document).on('click', '.btn-my-ajax', function (e) {
                var that = this;
                var options = $.extend({}, $(that).data() || {});
                if (typeof options.url === 'undefined' && $(that).attr("href")) {
                    options.url = $(that).attr("href");
                }
                options.url = Backend.api.replaceids(this, options.url);
                var success = typeof options.success === 'function' ? options.success : null;
                var error = typeof options.error === 'function' ? options.error : null;
                delete options.success;
                delete options.error;

                //如果未设备成功的回调,设定了自动刷新的情况下自动进行刷新
                if (!success && typeof options.tableId !== 'undefined' && typeof options.refresh !== 'undefined' && options.refresh) {
                    success = function () {
                        parent.$("#" + options.tableId).bootstrapTable('refresh');
                        setTimeout(function(){
                            Fast.api.close();
                        },1000)
                    }
                }
                if(!success && typeof options.refresh !== 'undefined' && options.refresh){
                    success = function () {
                        parent.$("#" + options.tableId).bootstrapTable('refresh');
                        setTimeout(function () {
                            window.location.reload();
                        },1000)
                    }
                }
                if (typeof options.confirm !== 'undefined') {
                    Layer.confirm(options.confirm, function (index) {
                        Backend.api.ajax(options, success, error);
                        Layer.close(index);
                    });
                } else {
                    Backend.api.ajax(options, success, error);
                }
                return false;
            })

            $(document).on('click', '.btn-my-dialog', function (e) {
                var that = this;
                var options = $.extend({}, $(that).data() || {});
                if (typeof options.url === 'undefined' && $(that).attr("href")) {
                    options.url = $(that).attr("href");
                }
                options.url = Backend.api.replaceids(this, options.url);
                var success = typeof options.success === 'function' ? options.success : null;
                var error = typeof options.error === 'function' ? options.error : null;
                delete options.success;
                delete options.error;

                //如果未设备成功的回调,设定了自动刷新的情况下自动进行刷新
                if (!success && typeof options.tableId !== 'undefined' && typeof options.refresh !== 'undefined' && options.refresh) {
                    success = function () {
                        parent.$("#" + options.tableId).bootstrapTable('refresh');
                        setTimeout(function(){
                            Fast.api.close();
                        },1000)
                    }
                }
                if(!success && typeof options.refresh !== 'undefined' && options.refresh){
                    success = function () {
                        parent.$("#" + options.tableId).bootstrapTable('refresh');
                        setTimeout(function () {
                            window.location.reload();
                        },1000)
                    }
                }
                if (typeof options.confirm !== 'undefined') {
                    Layer.confirm(options.confirm, function (index) {
                        Backend.api.ajax(options, success, error);
                        Layer.close(index);
                    });
                } else {
                    Backend.api.ajax(options, success, error);
                }
                return false;
            })
        },
        ext:function(){
            var lastProvince = '';
            //药厂选择器
            $('#c-pharmaceutical_factory_id').selectPage({
                showField: 'name',
                keyField: 'id',
                data: 'pharmaceutical/factory/index',
                //选择省份时，清空城市列表已选中的项目
                eSelect: function (data) {
                    if(data && lastProvince !== data.id){
                        $('#c-medicine_id').selectPageClear();
                    }
                },
                eAjaxSuccess: function (data) {
                    data.list = typeof data.rows !== 'undefined' ? data.rows : (typeof data.list !== 'undefined' ? data.list : []);
                    data.totalRow = typeof data.total !== 'undefined' ? data.total : (typeof data.totalRow !== 'undefined' ? data.totalRow : data.list.length);
                    return data;
                }
            });
            //城市选择器的初始化，使用空数据进行初始化
            $('#c-medicine_id').selectPage({
                showField: 'name',
                keyField: 'id',
                data: 'medicine/index',
                //向服务端提交的参数中，增加已选中的
                //设置返回-1，意为通知服务端返回空列表，初始化时使用
                params: function(){
                    var pid = $('#c-pharmaceutical_factory_id').val();
                    return pid ? {"custom[pharmaceutical_factory_id]":pid}:{};
                },
                eAjaxSuccess: function (data) {
                    data.list = typeof data.rows !== 'undefined' ? data.rows : (typeof data.list !== 'undefined' ? data.list : []);
                    data.totalRow = typeof data.total !== 'undefined' ? data.total : (typeof data.totalRow !== 'undefined' ? data.totalRow : data.list.length);
                    return data;
                }
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});