define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentationpatientchardrughistory/index',
                    add_url: 'presentationpatientchardrughistory/add',
                    edit_url: 'presentationpatientchardrughistory/edit',
                    del_url: 'presentationpatientchardrughistory/del',
                    multi_url: 'presentationpatientchardrughistory/multi',
                    table: 'presentation_patient_char_drug_history',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_patient_char_id', title: __('Presentation_patient_char_id')},
                        {field: 'name_of_drug_as_reported', title: __('Name_of_drug_as_reported')},
                        {field: 'mpid_version_date_number', title: __('Mpid_version_date_number')},
                        {field: 'medicinal_product_identifier_mpid', title: __('Medicinal_product_identifier_mpid')},
                        {field: 'phpid_version_date_number', title: __('Phpid_version_date_number')},
                        {field: 'pharmaceutical_product_identifier_phpid', title: __('Pharmaceutical_product_identifier_phpid')},
                        {field: 'start_date', title: __('Start_date')},
                        {field: 'end_date', title: __('End_date')},
                        {field: 'meddra_version_for_indication', title: __('Meddra_version_for_indication')},
                        {field: 'indication_meddra_code', title: __('Indication_meddra_code')},
                        {field: 'meddra_version_for_reaction', title: __('Meddra_version_for_reaction')},
                        {field: 'reaction_meddra_code', title: __('Reaction_meddra_code')},
                        {field: 'char.patient_name_or_initials', title: __('Char.patient_name_or_initials')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});