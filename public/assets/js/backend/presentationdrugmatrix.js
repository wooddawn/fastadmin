define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentationdrugmatrix/index',
                    add_url: 'presentationdrugmatrix/add',
                    edit_url: 'presentationdrugmatrix/edit',
                    del_url: 'presentationdrugmatrix/del',
                    multi_url: 'presentationdrugmatrix/multi',
                    table: 'presentation_drug_matrix',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_drug_id', title: __('Presentation_drug_id')},
                        {field: 'reactions_events_assessed', title: __('Reactions_events_assessed')},
                        {field: 'administration_and_start_of_reaction_event_number', title: __('Administration_and_start_of_reaction_event_number')},
                        {field: 'administration_and_start_of_reaction_event_unit', title: __('Administration_and_start_of_reaction_event_unit')},
                        {field: 'reaction_event_number', title: __('Reaction_event_number')},
                        {field: 'reaction_event_unit', title: __('Reaction_event_unit')},
                        {field: 'did_reaction_recur_on_re_administration', title: __('Did_reaction_recur_on_re_administration'), searchList: {"1 ":__('Did_reaction_recur_on_re_administration 1 '),"2 ":__('Did_reaction_recur_on_re_administration 2 '),"3 ":__('Did_reaction_recur_on_re_administration 3 '),"4 ":__('Did_reaction_recur_on_re_administration 4 ')}, formatter: Table.api.formatter.normal},
                        {field: 'drug.characterisation_of_drug_role', title: __('Drug.characterisation_of_drug_role')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});