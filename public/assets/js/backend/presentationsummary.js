define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentationsummary/index',
                    add_url: 'presentationsummary/add',
                    edit_url: 'presentationsummary/edit',
                    del_url: 'presentationsummary/del',
                    multi_url: 'presentationsummary/multi',
                    table: 'presentation_summary',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_id', title: __('Presentation_id')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
            Controller.api.showtable2();
            Controller.api.showtable3();

            $(document).on("click", ".infomaition", function (t) {
                var that = this;
                var numbers = $(that).data('number');
                Fast.api.open(
                    $(that).data('href'),
                    $(this).html()
                );
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            showtable2:function(){
                Table.api.init({
                    extend: {
                        index_url: '',
                        add_url: '',
                        edit_url: '',
                        del_url: '',
                        multi_url: '',
                        table: '',
                    }
                });
                var table2 = $("#table2");
                // 初始化表格
                table2.bootstrapTable({
                    url: '/admin/presentationsummarysender/index?sp_key=presentation_summary_id&sp_id='+Config.pid,
                    pk: 'id',
                    toolbar:'#toolbar2',
                    sortName: 'id',
                    columns: [
                        [
                            {field: 'id', title: __('Id')},
                            {field: 'senders_diagnosis_event_meddra_code', title: __('Senders_diagnosis_event_meddra_code')},
                            {field: 'senders_diagnosis_repeat_as_necessary', title: __('Senders_diagnosis_repeat_as_necessary')},
                        ]
                    ]
                });
                // 为表格绑定事件
                Table.api.bindevent(table2);
            },
            showtable3:function(){
                Table.api.init({
                    extend: {
                        index_url: '',
                        add_url: '',
                        edit_url: '',
                        del_url: '',
                        multi_url: '',
                        table: '',
                    }
                });
                var table3 = $("#table3");
                // 初始化表格
                table3.bootstrapTable({
                    url: '/admin/presentationsummarycomment/index?sp_key=presentation_summary_id&sp_id='+Config.pid,
                    pk: 'id',
                    toolbar:'#toolbar3',
                    sortName: 'id',
                    columns: [
                        [
                            {field: 'id', title: __('Id')},
                            {field: 'case_summary_and_reporters_comments_languageh', title: __('Case_summary_and_reporters_comments_languageh')},
                        ]
                    ]
                });
                // 为表格绑定事件
                Table.api.bindevent(table3);
            }
        }
    };
    return Controller;
});