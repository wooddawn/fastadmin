define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentationdrugidentifier/index',
                    add_url: 'presentationdrugidentifier/add',
                    edit_url: 'presentationdrugidentifier/edit',
                    del_url: 'presentationdrugidentifier/del',
                    multi_url: 'presentationdrugidentifier/multi',
                    table: 'presentation_drug_identifier',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_drug_id', title: __('Presentation_drug_id')},
                        {field: 'substance_specified_substance_name', title: __('Substance_specified_substance_name')},
                        {field: 'substancespecified_substance_termid_version_date_number', title: __('Substancespecified_substance_termid_version_date_number')},
                        {field: 'substancespecified_substance_termid', title: __('Substancespecified_substance_termid')},
                        {field: 'strength_number', title: __('Strength_number')},
                        {field: 'strength_unit', title: __('Strength_unit')},
                        {field: 'drug.characterisation_of_drug_role', title: __('Drug.characterisation_of_drug_role')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});