define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentation/patient/char/parent/information/index',
                    add_url: 'presentation/patient/char/parent/information/add',
                    edit_url: 'presentation/patient/char/parent/information/edit',
                    del_url: 'presentation/patient/char/parent/information/del',
                    multi_url: 'presentation/patient/char/parent/information/multi',
                    table: 'presentation_patient_char_parent_information',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_patient_char_id', title: __('Presentation_patient_char_id')},
                        {field: 'parent_identification', title: __('Parent_identification')},
                        {field: 'date_of_birth_of_parent', title: __('Date_of_birth_of_parent')},
                        {field: 'age_of_parent', title: __('Age_of_parent')},
                        {field: 'age_of_parent_number', title: __('Age_of_parent_number')},
                        {field: 'age_of_parent_unit', title: __('Age_of_parent_unit')},
                        {field: 'last_menstrual_period_date', title: __('Last_menstrual_period_date')},
                        {field: 'body_weight_kg_of_parent', title: __('Body_weight_kg_of_parent')},
                        {field: 'height_cm_of_parent', title: __('Height_cm_of_parent')},
                        {field: 'sex_of_parentd', title: __('Sex_of_parentd'), searchList: {"1":__('Sex_of_parentd 1'),"2":__('Sex_of_parentd 2')}, formatter: Table.api.formatter.normal},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});