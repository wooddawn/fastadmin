define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentation/patient/char/medical/istory/index',
                    add_url: 'presentation/patient/char/medical/istory/add',
                    edit_url: 'presentation/patient/char/medical/istory/edit',
                    del_url: 'presentation/patient/char/medical/istory/del',
                    multi_url: 'presentation/patient/char/medical/istory/multi',
                    table: 'presentation_patient_char_medical_istory',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_patient_char_id', title: __('Presentation_patient_char_id')},
                        {field: 'meddra_version_for_medical_history', title: __('Meddra_version_for_medical_history')},
                        {field: 'medical_history_disease_surgical_procedure_etc_meddra_code', title: __('Medical_history_disease_surgical_procedure_etc_meddra_code')},
                        {field: 'start_date', title: __('Start_date')},
                        {field: 'continuing', title: __('Continuing')},
                        {field: 'end_date', title: __('End_date')},
                        {field: 'family_historyd', title: __('Family_historyd')},
                        {field: 'char.patient_name_or_initials', title: __('Char.patient_name_or_initials')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});