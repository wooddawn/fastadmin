define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentation/patient/char/reported/death/index',
                    add_url: 'presentation/patient/char/reported/death/add',
                    edit_url: 'presentation/patient/char/reported/death/edit',
                    del_url: 'presentation/patient/char/reported/death/del',
                    multi_url: 'presentation/patient/char/reported/death/multi',
                    table: 'presentation_patient_char_reported_death',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_patient_char_id', title: __('Presentation_patient_char_id')},
                        {field: 'meddra_version_for_reported_causes_of_death', title: __('Meddra_version_for_reported_causes_of_death')},
                        {field: 'reported_causes_of_death_meddra_code', title: __('Reported_causes_of_death_meddra_code')},
                        {field: 'reported_causes_of_death_free_text', title: __('Reported_causes_of_death_free_text')},
                        {field: 'char.patient_name_or_initials', title: __('Char.patient_name_or_initials')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});