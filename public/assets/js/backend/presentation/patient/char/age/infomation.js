define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentation/patient/char/age/infomation/index',
                    add_url: 'presentation/patient/char/age/infomation/add',
                    edit_url: 'presentation/patient/char/age/infomation/edit',
                    del_url: 'presentation/patient/char/age/infomation/del',
                    multi_url: 'presentation/patient/char/age/infomation/multi',
                    table: 'presentation_patient_char_age_infomation',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_patient_char_id', title: __('Presentation_patient_char_id')},
                        {field: 'date_of_birth', title: __('Date_of_birth')},
                        {field: 'age_at_time_of_onset_of_reaction_event_number', title: __('Age_at_time_of_onset_of_reaction_event_number')},
                        {field: 'age_at_time_of_onset_of_reaction_event_unit', title: __('Age_at_time_of_onset_of_reaction_event_unit')},
                        {field: 'foetus_number', title: __('Foetus_number')},
                        {field: 'foetus_unit', title: __('Foetus_unit')},
                        {field: 'patient_age_group_as_per_reporter', title: __('Patient_age_group_as_per_reporter'), searchList: {"0":__('Patient_age_group_as_per_reporter 0'),"1":__('Patient_age_group_as_per_reporter 1'),"2":__('Patient_age_group_as_per_reporter 2'),"3":__('Patient_age_group_as_per_reporter 3'),"4":__('Patient_age_group_as_per_reporter 4'),"5":__('Patient_age_group_as_per_reporter 5'),"6":__('Patient_age_group_as_per_reporter 6')}, formatter: Table.api.formatter.normal},
                        {field: 'char.patient_name_or_initials', title: __('Char.patient_name_or_initials')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});