define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentation/patient/char/index',
                    add_url: 'presentation/patient/char/add',
                    edit_url: 'presentation/patient/char/edit',
                    del_url: 'presentation/patient/char/del',
                    multi_url: 'presentation/patient/char/multi',
                    table: 'presentation_patient_char',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_id', title: __('Presentation_id')},
                        {field: 'patient_name_or_initials', title: __('Patient_name_or_initials')},
                        {field: 'gp_medical_record_number', title: __('Gp_medical_record_number')},
                        {field: 'specialist_record_number', title: __('Specialist_record_number')},
                        {field: 'hospital_record_number', title: __('Hospital_record_number')},
                        {field: 'investigation_number', title: __('Investigation_number')},
                        {field: 'body_weight_kg', title: __('Body_weight_kg')},
                        {field: 'height_cm', title: __('Height_cm')},
                        {field: 'sex', title: __('Sex'), searchList: {"1":__('Sex 1'),"2":__('Sex 2')}, formatter: Table.api.formatter.normal},
                        {field: 'last_menstrual_period_date', title: __('Last_menstrual_period_date')},
                        {field: 'reaction_event', title: __('Reaction_event')},
                        {field: 'concomitant_therapies', title: __('Concomitant_therapies'), searchList: {"1":__('Concomitant_therapies 1'),"0":__('Concomitant_therapies 0')}, formatter: Table.api.formatter.normal},
                        {field: 'date_of_death', title: __('Date_of_death')},
                        {field: 'was_autopsy_done', title: __('Was_autopsy_done'), searchList: {"1":__('Was_autopsy_done 1'),"0":__('Was_autopsy_done 0')}, formatter: Table.api.formatter.normal},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});