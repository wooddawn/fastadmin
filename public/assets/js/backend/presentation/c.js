define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentation/c/index',
                    add_url: 'presentation/c/add',
                    edit_url: 'presentation/c/edit',
                    del_url: 'presentation/c/del',
                    multi_url: 'presentation/c/multi',
                    table: 'presentation_c',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'uuid', title: __('Uuid')},
                        {field: 'createdate', title: __('Createdate')},
                        {field: 'type', title: __('Type'), searchList: {"1":__('Type 1'),"2":__('Type 2'),"3 ":__('Type 3 '),"4":__('Type 4')}, formatter: Table.api.formatter.normal},
                        {field: 'fristdate', title: __('Fristdate'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'newdate', title: __('Newdate'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'has_file', title: __('Has_file'), searchList: {"true":__('Has_file true'),"false":__('Has_file false')}, formatter: Table.api.formatter.normal},
                        {field: 'files', title: __('Files')},
                        {field: 'is_quick', title: __('Is_quick'), searchList: {"true":__('True'),"false":__('False'),"null":__('Null')}, formatter: Table.api.formatter.normal},
                        {field: 'worldwideuuid', title: __('Worldwideuuid')},
                        {field: 'frist_sender', title: __('Frist_sender'), searchList: {"1":__('Frist_sender 1'),"2":__('Frist_sender 2')}, formatter: Table.api.formatter.normal},
                        {field: 'other_case', title: __('Other_case'), searchList: {"true":__('True'),"null":__('Null')}, formatter: Table.api.formatter.normal},
                        {field: 'is_report', title: __('Is_report'), searchList: {"1":__('Is_report 1'),"2":__('Is_report 2')}, formatter: Table.api.formatter.normal},
                        {field: 'presentation_id', title: __('Presentation_id')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});