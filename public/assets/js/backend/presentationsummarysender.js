define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentationsummarysender/index',
                    add_url: 'presentationsummarysender/add',
                    edit_url: 'presentationsummarysender/edit',
                    del_url: 'presentationsummarysender/del',
                    multi_url: 'presentationsummarysender/multi',
                    table: 'presentation_summary_sender',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_summary_id', title: __('Presentation_summary_id')},
                        {field: 'senders_diagnosis_event_meddra_code', title: __('Senders_diagnosis_event_meddra_code')},
                        {field: 'senders_diagnosis_repeat_as_necessary', title: __('Senders_diagnosis_repeat_as_necessary')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});