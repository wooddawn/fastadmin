define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentationdrugdosage/index',
                    add_url: 'presentationdrugdosage/add',
                    edit_url: 'presentationdrugdosage/edit',
                    del_url: 'presentationdrugdosage/del',
                    multi_url: 'presentationdrugdosage/multi',
                    table: 'presentation_drug_dosage',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_drug_id', title: __('Presentation_drug_id')},
                        {field: 'dose_number', title: __('Dose_number')},
                        {field: 'dose_unit', title: __('Dose_unit')},
                        {field: 'number_of_units_in_the_interval', title: __('Number_of_units_in_the_interval')},
                        {field: 'definition_of_the_time_interval_unit', title: __('Definition_of_the_time_interval_unit')},
                        {field: 'date_and_time_of_start_of_drug', title: __('Date_and_time_of_start_of_drug')},
                        {field: 'date_and_time_of_last_administration', title: __('Date_and_time_of_last_administration')},
                        {field: 'duration_of_drug_administration_number', title: __('Duration_of_drug_administration_number')},
                        {field: 'duration_of_drug_administration_unit', title: __('Duration_of_drug_administration_unit')},
                        {field: 'batch_lot_number', title: __('Batch_lot_number')},
                        {field: 'pharmaceutical_dose_form_free_text', title: __('Pharmaceutical_dose_form_free_text')},
                        {field: 'pharmaceutical_dose_form_termid_version_date_number', title: __('Pharmaceutical_dose_form_termid_version_date_number')},
                        {field: 'pharmaceutical_dose_form_termid', title: __('Pharmaceutical_dose_form_termid')},
                        {field: 'route_of_administration_free_text', title: __('Route_of_administration_free_text')},
                        {field: 'route_of_administration_termid_version_date_number', title: __('Route_of_administration_termid_version_date_number')},
                        {field: 'route_of_administration_termid', title: __('Route_of_administration_termid')},
                        {field: 'parent_route_of_administration_free_text', title: __('Parent_route_of_administration_free_text')},
                        {field: 'parent_route_of_administration_termid_version_date_number', title: __('Parent_route_of_administration_termid_version_date_number')},
                        {field: 'parent_route_of_administration_termidg', title: __('Parent_route_of_administration_termidg')},
                        {field: 'drug.characterisation_of_drug_role', title: __('Drug.characterisation_of_drug_role')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});