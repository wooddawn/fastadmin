define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentationdrugcase/index',
                    add_url: 'presentationdrugcase/add',
                    edit_url: 'presentationdrugcase/edit',
                    del_url: 'presentationdrugcase/del',
                    multi_url: 'presentationdrugcase/multi',
                    table: 'presentation_drug_case',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_drug_id', title: __('Presentation_drug_id')},
                        {field: 'indication_as_reported_by_the_primary_source', title: __('Indication_as_reported_by_the_primary_source')},
                        {field: 'meddra_version_for_indication', title: __('Meddra_version_for_indication')},
                        {field: 'indication_meddra_code', title: __('Indication_meddra_code')},
                        {field: 'drug.characterisation_of_drug_role', title: __('Drug.characterisation_of_drug_role')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});