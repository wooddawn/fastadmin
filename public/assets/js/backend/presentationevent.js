define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentationevent/index',
                    add_url: 'presentationevent/add/pid/'+Config.pid,
                    edit_url: 'presentationevent/edit',
                    del_url: 'presentationevent/del',
                    multi_url: 'presentationevent/multi',
                    table: 'presentationEvent',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_id', title: __('Presentation_id')},
                        {field: 'native_language', title: __('Native_language')},
                        {field: 'source_language', title: __('Source_language')},
                        {field: 'translation', title: __('Translation')},
                        {field: 'meddra_version', title: __('Meddra_version')},
                        {field: 'meddra_code', title: __('Meddra_code')},
                        {field: 'highlighted', title: __('Highlighted'), searchList: {"1":__('Highlighted 1'),"2":__('Highlighted 2'),"3":__('Highlighted 3'),"4":__('Highlighted 4')}, formatter: Table.api.formatter.normal},
                        {field: 'event_level', title: __('Event_level')},
                        {field: 'death', title: __('Death'), searchList: {"1":__('Death 1'),"0":__('Death 0')}, formatter: Table.api.formatter.normal},
                        {field: 'life_threatening', title: __('Life_threatening'), searchList: {"1":__('Life_threatening 1'),"0":__('Life_threatening 0')}, formatter: Table.api.formatter.normal},
                        {field: 'hospitalisation', title: __('Hospitalisation'), searchList: {"1":__('Hospitalisation 1'),"0":__('Hospitalisation 0')}, formatter: Table.api.formatter.normal},
                        {field: 'incapacitating', title: __('Incapacitating'), searchList: {"1":__('Incapacitating 1'),"0":__('Incapacitating 0')}, formatter: Table.api.formatter.normal},
                        {field: 'birth_defect', title: __('Birth_defect'), searchList: {"1":__('Birth_defect 1'),"0":__('Birth_defect 0')}, formatter: Table.api.formatter.normal},
                        {field: 'other_condition', title: __('Other_condition'), searchList: {"1":__('Other_condition 1'),"0":__('Other_condition 0')}, formatter: Table.api.formatter.normal},
                        {field: 'startdate', title: __('Startdate')},
                        {field: 'enddate', title: __('Enddate')},
                        {field: 'duration', title: __('Duration')},
                        {field: 'duration_unit', title: __('Duration_unit')},
                        {field: 'outcome', title: __('Outcome'), searchList: {"1":__('Outcome 1'),"2":__('Outcome 2'),"3":__('Outcome 3'),"4":__('Outcome 4'),"5":__('Outcome 5'),"0":__('Outcome 0')}, formatter: Table.api.formatter.normal},
                        {field: 'medical_confirmation', title: __('Medical_confirmation'), searchList: {"1":__('Medical_confirmation 1'),"0":__('Medical_confirmation 0')}, formatter: Table.api.formatter.normal},
                        {field: 'country_id', title: __('Country_id')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});