define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'Presentationdrugmatrixassessment/index',
                    add_url: 'Presentationdrugmatrixassessment/add',
                    edit_url: 'Presentationdrugmatrixassessment/edit',
                    del_url: 'Presentationdrugmatrixassessment/del',
                    multi_url: 'Presentationdrugmatrixassessment/multi',
                    table: 'presentation_drug_matrix_assessment',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_drug_matrix_id', title: __('Presentation_drug_matrix_id')},
                        {field: 'source_of_assessment', title: __('Source_of_assessment')},
                        {field: 'method_of_assessment', title: __('Method_of_assessment')},
                        {field: 'result_of_assessmentg', title: __('Result_of_assessmentg')},
                        {field: 'matrix.reactions_events_assessed', title: __('Matrix.reactions_events_assessed')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});