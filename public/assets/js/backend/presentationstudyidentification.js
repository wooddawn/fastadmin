define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentationstudyidentification/index',
                    add_url: 'presentationstudyidentification/add/pid/'+Config.pid,
                    edit_url: 'presentationstudyidentification/edit',
                    del_url: 'presentationstudyidentification/del',
                    multi_url: 'presentationstudyidentification/multi',
                    table: 'presentationStudyIdentification',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_id', title: __('Presentation_id')},
                        {field: 'studyname', title: __('Studyname')},
                        {field: 'studynumber', title: __('Studynumber')},
                        {field: 'studytype', title: __('Studytype'), searchList: {"1":__('Studytype 1'),"2":__('Studytype 2'),"3 ":__('Studytype 3 ')}, formatter: Table.api.formatter.normal},
                        {field: 'registration_number', title: __('Registration_number')},
                        {field: 'registration_country', title: __('Registration_country')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});