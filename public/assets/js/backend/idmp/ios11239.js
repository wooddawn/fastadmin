define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'idmp/ios11239/index',
                    add_url: 'idmp/ios11239/add',
                    edit_url: 'idmp/ios11239/edit',
                    del_url: 'idmp/ios11239/del',
                    multi_url: 'idmp/ios11239/multi',
                    table: 'idmp_ios11239',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'term', title: __('Term')},
                        {field: 'lang', title: __('Lang')},
                        {field: 'role', title: __('Role')},
                        {field: 'code', title: __('Code')},
                        {field: 'type', title: __('Type')},
                        {field: 'source', title: __('Source')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});