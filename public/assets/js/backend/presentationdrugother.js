define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'presentationdrugother/index',
                    add_url: 'presentationdrugother/add',
                    edit_url: 'presentationdrugother/edit',
                    del_url: 'presentationdrugother/del',
                    multi_url: 'presentationdrugother/multi',
                    table: 'presentation_drug_other',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'presentation_drug_id', title: __('Presentation_drug_id')},
                        {field: 'additional_information_on_drug_coded', title: __('Additional_information_on_drug_coded'), searchList: {"1":__('Additional_information_on_drug_coded 1'),"2":__('Additional_information_on_drug_coded 2'),"3":__('Additional_information_on_drug_coded 3'),"4":__('Additional_information_on_drug_coded 4'),"5":__('Additional_information_on_drug_coded 5'),"6":__('Additional_information_on_drug_coded 6'),"7":__('Additional_information_on_drug_coded 7'),"8":__('Additional_information_on_drug_coded 8'),"9":__('Additional_information_on_drug_coded 9'),"10":__('Additional_information_on_drug_coded 10'),"11":__('Additional_information_on_drug_coded 11')}, formatter: Table.api.formatter.normal},
                        {field: 'drug.characterisation_of_drug_role', title: __('Drug.characterisation_of_drug_role')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});