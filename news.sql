CREATE TABLE `fa_pharmaceutical_factory` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`name` varchar(255) NOT NULL COMMENT '药厂名',
`status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '状态:0=隐藏,1=正常',
`createtime` int NULL COMMENT '创建时间',
`updatetime` int NULL COMMENT '更新时间',
PRIMARY KEY (`id`) 
)
COMMENT = '药厂';

CREATE TABLE `fa_medicine` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`createtime` int(10) NULL COMMENT '创建时间',
`updatetime` int(10) NULL DEFAULT 更新时间,
`status` enum('0','1') NULL DEFAULT '1' COMMENT '状态:0=隐藏,1=正常',
`weigh` int(2) NULL COMMENT '排序',
`pharmaceutical_factory_id` int(10) NULL COMMENT '药厂',
PRIMARY KEY (`id`) 
)
COMMENT = '药品';

CREATE TABLE `fa_presentation` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`pharmaceutical_factory_id` int(10) NULL COMMENT '药厂名',
`medicine_id` int(10) NULL COMMENT '药品名',
`name` varchar(255) NULL COMMENT '患者姓名',
`market` enum('0','1') NULL DEFAULT '0' COMMENT '状态:0=未上市,1=已经上市',
`createtime` int(10) NULL COMMENT '创建时间',
`updatetime` int(10) NULL COMMENT '更新时间',
`date` date NULL DEFAULT 报告日期,
`isindex` enum('0','1') NULL DEFAULT '0' COMMENT '是否首次:0=首次,1=随访',
`status` int NULL COMMENT '状态',
`doctor` varchar(255) NULL COMMENT '生医',
`pdffile` varchar(255) NULL COMMENT '报告文件',
PRIMARY KEY (`id`) 
)
COMMENT = '报告';

CREATE TABLE `fa_medicine_attr` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`medicine_id` int(10) NOT NULL COMMENT '药品ID',
`reaction` varchar(255) NULL COMMENT '不良反应事件'
);

CREATE TABLE `fa_admin` (
`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
`username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
`nickname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '昵称',
`password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码',
`salt` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码盐',
`avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '头像',
`email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '电子邮箱',
`loginfailure` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '失败次数',
`logintime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '登录时间',
`createtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
`updatetime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
`token` varchar(59) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Session标识',
`status` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'normal' COMMENT '状态',
PRIMARY KEY (`id`) ,
UNIQUE INDEX `username` (`username` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 6
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '管理员表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_admin_log` (
`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
`admin_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员ID',
`username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员名字',
`url` varchar(1500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作页面',
`title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '日志标题',
`content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容',
`ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'IP',
`useragent` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'User-Agent',
`createtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '操作时间',
PRIMARY KEY (`id`) ,
INDEX `name` (`username` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 171
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '管理员日志表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_area` (
`id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
`pid` int(10) NULL DEFAULT NULL COMMENT '父id',
`shortname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '简称',
`name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
`mergename` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '全称',
`level` tinyint(4) NULL DEFAULT NULL COMMENT '层级 0 1 2 省市区县',
`pinyin` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拼音',
`code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '长途区号',
`zip` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮编',
`first` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '首字母',
`lng` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '经度',
`lat` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '纬度',
PRIMARY KEY (`id`) ,
INDEX `pid` (`pid` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 3749
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '地区表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Dynamic;

CREATE TABLE `fa_attachment` (
`id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
`admin_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员ID',
`user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员ID',
`url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '物理路径',
`imagewidth` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '宽度',
`imageheight` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '高度',
`imagetype` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图片类型',
`imageframes` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图片帧数',
`filesize` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件大小',
`mimetype` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'mime类型',
`extparam` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '透传数据',
`createtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建日期',
`updatetime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
`uploadtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上传时间',
`storage` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'local' COMMENT '存储位置',
`sha1` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 2
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '附件表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_auth_group` (
`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
`pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父组别',
`name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '组名',
`rules` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '规则ID',
`createtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
`updatetime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
`status` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '状态',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 11
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '分组表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_auth_group_access` (
`uid` int(10) UNSIGNED NOT NULL COMMENT '会员ID',
`group_id` int(10) UNSIGNED NOT NULL COMMENT '级别ID',
UNIQUE INDEX `uid_group_id` (`uid` ASC, `group_id` ASC) USING BTREE,
INDEX `uid` (`uid` ASC) USING BTREE,
INDEX `group_id` (`group_id` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 0
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '权限分组表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_auth_rule` (
`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
`type` enum('menu','file') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'file' COMMENT 'menu为菜单,file为权限节点',
`pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父ID',
`name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规则名称',
`title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规则名称',
`icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图标',
`condition` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '条件',
`remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注',
`ismenu` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否为菜单',
`createtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
`updatetime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
`weigh` int(10) NOT NULL DEFAULT 0 COMMENT '权重',
`status` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '状态',
PRIMARY KEY (`id`) ,
UNIQUE INDEX `name` (`name` ASC) USING BTREE,
INDEX `pid` (`pid` ASC) USING BTREE,
INDEX `weigh` (`weigh` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 182
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '节点表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_category` (
`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
`pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父ID',
`type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '栏目类型',
`name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
`nickname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
`flag` set('hot','index','recommend') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
`image` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图片',
`keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '关键字',
`description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '描述',
`diyname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '自定义名称',
`createtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
`updatetime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
`weigh` int(10) NOT NULL DEFAULT 0 COMMENT '权重',
`status` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '状态',
PRIMARY KEY (`id`) ,
INDEX `weigh` (`weigh` ASC, `id` ASC) USING BTREE,
INDEX `pid` (`pid` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 14
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '分类表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_cms_addonnews` (
`id` int(10) NOT NULL,
`content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`author` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '作者',
`area` enum('domestic','overseas','local') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'domestic' COMMENT '地区',
`device` enum('vr','ar') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'vr' COMMENT '设备',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 0
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '新闻'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_cms_addonproduct` (
`id` int(10) NOT NULL,
`content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`productdata` varchar(1500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '产品列表',
`Index` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '111' COMMENT '111',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 0
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '产品表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_cms_archives` (
`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
`channel_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '栏目ID',
`model_id` int(10) NOT NULL DEFAULT 0 COMMENT '模型ID',
`title` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文章标题',
`flag` set('hot','new','recommend') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标志',
`image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '缩略图',
`keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '关键字',
`description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '描述',
`tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'TAG',
`weigh` int(10) NOT NULL DEFAULT 0 COMMENT '权重',
`views` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '浏览次数',
`comments` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '评论次数',
`likes` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '点赞数',
`dislikes` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '点踩数',
`diyname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '自定义URL',
`createtime` int(10) NULL DEFAULT NULL COMMENT '创建时间',
`updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
`publishtime` int(10) NULL DEFAULT NULL COMMENT '发布时间',
`deletetime` int(10) NULL DEFAULT NULL COMMENT '删除时间',
`status` enum('normal','hidden') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'normal' COMMENT '状态',
PRIMARY KEY (`id`) ,
INDEX `status` (`channel_id` ASC, `status` ASC) USING BTREE,
INDEX `channel` (`channel_id` ASC, `weigh` ASC, `id` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 36
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '内容表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_cms_block` (
`id` smallint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
`type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '类型',
`name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '名称',
`title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标题',
`image` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图片',
`url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '链接',
`content` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
`createtime` int(10) NULL DEFAULT NULL COMMENT '添加时间',
`updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
`status` enum('normal','hidden') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'normal' COMMENT '状态',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 10
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '区块表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_cms_channel` (
`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
`type` enum('channel','page','link','list') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型',
`model_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '模型ID',
`parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父ID',
`name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '名称',
`image` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图片',
`flag` set('hot','new','recommend') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '标志',
`keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '关键字',
`description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '描述',
`diyname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '自定义名称',
`outlink` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '外部链接',
`items` mediumint(8) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文章数量',
`weigh` int(10) NOT NULL DEFAULT 0 COMMENT '权重',
`channeltpl` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '栏目页模板',
`listtpl` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '列表页模板',
`showtpl` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '详情页模板',
`pagesize` smallint(5) NOT NULL DEFAULT 0 COMMENT '分页大小',
`createtime` int(10) NULL DEFAULT NULL COMMENT '创建时间',
`updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
`status` enum('normal','hidden') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'normal' COMMENT '状态',
PRIMARY KEY (`id`) ,
UNIQUE INDEX `diyname` (`diyname` ASC) USING BTREE,
INDEX `weigh` (`weigh` ASC, `id` ASC) USING BTREE,
INDEX `parent_id` (`parent_id` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 45
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '栏目表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_cms_comment` (
`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
`user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员ID',
`type` enum('archives','page') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'archives' COMMENT '类型',
`aid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '关联ID',
`pid` int(10) NOT NULL DEFAULT 0 COMMENT '父ID',
`content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
`comments` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '评论数',
`ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'IP',
`useragent` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'User Agent',
`subscribe` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '订阅',
`createtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
`updatetime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
`status` enum('normal','hidden') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'normal' COMMENT '状态',
PRIMARY KEY (`id`) ,
INDEX `post_id` (`aid` ASC, `pid` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 8
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '评论表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_cms_fields` (
`id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
`model_id` int(10) NOT NULL DEFAULT 0 COMMENT '模型ID',
`name` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '名称',
`type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '类型',
`title` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标题',
`content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
`defaultvalue` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '默认值',
`rule` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '验证规则',
`msg` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '错误消息',
`ok` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '成功消息',
`tip` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '提示消息',
`decimals` tinyint(1) NULL DEFAULT NULL COMMENT '小数点',
`length` mediumint(8) NULL DEFAULT NULL COMMENT '长度',
`minimum` smallint(6) NULL DEFAULT NULL COMMENT '最小数量',
`maximum` smallint(6) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最大数量',
`extend` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '扩展信息',
`weigh` int(10) NOT NULL DEFAULT 0 COMMENT '排序',
`createtime` int(10) NULL DEFAULT NULL COMMENT '添加时间',
`updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
`isfilter` tinyint(1) NOT NULL DEFAULT 0 COMMENT '筛选',
`status` enum('normal','hidden') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态',
PRIMARY KEY (`id`) ,
INDEX `model_id` (`model_id` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 143
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '模型字段表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_cms_model` (
`id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
`name` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '模型名称',
`table` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '表名',
`fields` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '字段列表',
`channeltpl` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '栏目页模板',
`listtpl` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '列表页模板',
`showtpl` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '详情页模板',
`createtime` int(10) NULL DEFAULT NULL COMMENT '创建时间',
`updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
`setting` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '模型配置',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '内容模型表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_cms_page` (
`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
`category_id` int(10) NOT NULL DEFAULT 0 COMMENT '分类ID',
`type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '类型',
`title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标题',
`keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '关键字',
`description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '描述',
`flag` set('hot','index','recommend') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标志',
`image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '头像',
`content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
`icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图标',
`views` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '点击',
`comments` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '评论',
`diyname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '自定义',
`showtpl` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '视图模板',
`createtime` int(10) NULL DEFAULT NULL COMMENT '创建时间',
`updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
`weigh` int(10) NOT NULL DEFAULT 0 COMMENT '权重',
`status` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '状态',
PRIMARY KEY (`id`) ,
INDEX `type` (`type` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 29
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '单页表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_cms_tags` (
`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
`name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标签名称',
`archives` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '文档ID集合',
`nums` int(10) UNSIGNED NOT NULL DEFAULT 0,
PRIMARY KEY (`id`) ,
UNIQUE INDEX `name` (`name` ASC) USING BTREE,
INDEX `nums` (`nums` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 22
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '标签表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Dynamic;

CREATE TABLE `fa_command` (
`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
`type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '类型',
`params` varchar(1500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '参数',
`command` varchar(1500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '命令',
`content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '返回结果',
`executetime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '执行时间',
`createtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
`updatetime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
`status` enum('successed','failured') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'failured' COMMENT '状态',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 1
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '在线命令表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_config` (
`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
`name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '变量名',
`group` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分组',
`title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '变量标题',
`tip` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '变量描述',
`type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '类型:string,text,int,bool,array,datetime,date,file',
`value` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '变量值',
`content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '变量字典数据',
`rule` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '验证规则',
`extend` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '扩展属性',
PRIMARY KEY (`id`) ,
UNIQUE INDEX `name` (`name` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 18
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '系统配置'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_ems` (
`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
`event` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '事件',
`email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '邮箱',
`code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '验证码',
`times` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '验证次数',
`ip` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'IP',
`createtime` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '创建时间',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 1
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '邮箱验证码表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_reaction` (
`id` int(10) NOT NULL,
`content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '药厂名',
`status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 0
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '药厂'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Dynamic;

CREATE TABLE `fa_sms` (
`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
`event` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '事件',
`mobile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机号',
`code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '验证码',
`times` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '验证次数',
`ip` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'IP',
`createtime` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '创建时间',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 1
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '短信验证码表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_test` (
`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
`admin_id` int(10) NOT NULL DEFAULT 0 COMMENT '管理员ID',
`category_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '分类ID(单选)',
`category_ids` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类ID(多选)',
`week` enum('monday','tuesday','wednesday') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '星期(单选):monday=星期一,tuesday=星期二,wednesday=星期三',
`flag` set('hot','index','recommend') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标志(多选):hot=热门,index=首页,recommend=推荐',
`genderdata` enum('male','female') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'male' COMMENT '性别(单选):male=男,female=女',
`hobbydata` set('music','reading','swimming') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '爱好(多选):music=音乐,reading=读书,swimming=游泳',
`title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标题',
`content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容',
`image` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图片',
`images` varchar(1500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图片组',
`attachfile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '附件',
`keywords` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '关键字',
`description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '描述',
`city` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '省市',
`price` float(10,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '价格',
`views` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '点击',
`startdate` date NULL DEFAULT NULL COMMENT '开始日期',
`activitytime` datetime NULL DEFAULT NULL COMMENT '活动时间(datetime)',
`year` year NULL DEFAULT NULL COMMENT '年',
`times` time NULL DEFAULT NULL COMMENT '时间',
`refreshtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '刷新时间(int)',
`createtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
`updatetime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
`weigh` int(10) NOT NULL DEFAULT 0 COMMENT '权重',
`switch` tinyint(1) NOT NULL DEFAULT 0 COMMENT '开关',
`status` enum('normal','hidden') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'normal' COMMENT '状态',
`state` enum('0','1','2') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '状态值:0=禁用,1=正常,2=推荐',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 2
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '测试表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_user` (
`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
`group_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '组别ID',
`username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
`nickname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '昵称',
`password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码',
`salt` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码盐',
`email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '电子邮箱',
`mobile` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机号',
`avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '头像',
`level` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '等级',
`gender` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '性别',
`birthday` date NULL DEFAULT NULL COMMENT '生日',
`bio` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '格言',
`score` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '积分',
`successions` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT '连续登录天数',
`maxsuccessions` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT '最大连续登录天数',
`prevtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上次登录时间',
`logintime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '登录时间',
`loginip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '登录IP',
`loginfailure` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '失败次数',
`joinip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '加入IP',
`jointime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '加入时间',
`createtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
`updatetime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
`token` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Token',
`status` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '状态',
`verification` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '验证',
PRIMARY KEY (`id`) ,
INDEX `username` (`username` ASC) USING BTREE,
INDEX `email` (`email` ASC) USING BTREE,
INDEX `mobile` (`mobile` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 2
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '会员表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_user_group` (
`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
`name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '组名',
`rules` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '权限节点',
`createtime` int(10) NULL DEFAULT NULL COMMENT '添加时间',
`updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
`status` enum('normal','hidden') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 2
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '会员组表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_user_rule` (
`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
`pid` int(10) NULL DEFAULT NULL COMMENT '父ID',
`name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
`title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '标题',
`remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
`ismenu` tinyint(1) NULL DEFAULT NULL COMMENT '是否菜单',
`createtime` int(10) NULL DEFAULT NULL COMMENT '创建时间',
`updatetime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
`weigh` int(10) NULL DEFAULT 0 COMMENT '权重',
`status` enum('normal','hidden') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 13
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '会员规则表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_user_score_log` (
`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
`user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员ID',
`score` int(10) NOT NULL DEFAULT 0 COMMENT '变更积分',
`before` int(10) NOT NULL DEFAULT 0 COMMENT '变更前积分',
`after` int(10) NOT NULL DEFAULT 0 COMMENT '变更后积分',
`memo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注',
`createtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 1
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '会员积分变动表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_user_token` (
`token` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Token',
`user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员ID',
`createtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
`expiretime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '过期时间',
PRIMARY KEY (`token`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 0
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '会员Token表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

CREATE TABLE `fa_version` (
`id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
`oldversion` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '旧版本号',
`newversion` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '新版本号',
`packagesize` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '包大小',
`content` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '升级内容',
`downloadurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '下载地址',
`enforce` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '强制更新',
`createtime` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
`updatetime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
`weigh` int(10) NOT NULL DEFAULT 0 COMMENT '权重',
`status` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '状态',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 2
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = '版本表'
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Compact;

