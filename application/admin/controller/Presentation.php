<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use app\admin\model\Admin;
use app\admin\model\Presentationcomment;

/**
 * 报告
 *
 * @icon fa fa-circle-o
 */
class Presentation extends Backend
{

    /**
     * Presentation模型对象
     * @var \app\admin\model\Presentation
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Presentation;
        $this->view->assign("marketList", $this->model->getMarketList());
        $this->view->assign("isindexList", $this->model->getIsindexList());
        $this->view->assign("statusList", $this->model->getStatusList());
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */


    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        $type = (int)$this->request->request('type');
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $op = $type==0 ? 'in':'=';
            $params = $type==0 ? '(0,1)':$type;
            $total = $this->model
                ->with(['medicine','factory'])
                ->where($where)
                ->where('presentation.status', $op, $params)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->with(['medicine','factory'])
                ->where($where)
                ->where('presentation.status', $op, $params)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            foreach ($list as $row) {

                $row->getRelation('medicine')->visible(['name']);
                $row->getRelation('factory')->visible(['name']);
            }
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

    public function check0()
    {
        return $this->view->fetch();
    }

    public function check1()
    {
        return $this->view->fetch();
    }

    public function check2()
    {
        return $this->view->fetch();
    }

    public function check3()
    {
        return $this->view->fetch();
    }

    public function check4()
    {
        return $this->view->fetch();
    }

    public function check5()
    {
        return $this->view->fetch();
    }

    /**
     * 审核
     */
    public function check()
    {
        //当前是否为关联查询
        //设置过滤方法
        if (true || $this->request->isAjax()) {
            $data = [];
            $type = intval($this->request->request('type'));
            $ids = intval($this->request->request('ids'));
            $row = $this->model->get($ids);
            if (empty($row)) {
                $this->error('报告不存在!');
            }
            if ($type == 1) {
                $data['status1'] = $this->auth->id;
                $data['status'] = 1;

                if ($row['status1'] != '') {
                    $this->error('该报告已经被领取了');
                } else {
                    $row->save($data);
                    $this->success('领取成功');
                }
            }
            if ($type == 2) {
                $data['status'] = 2;
                if ($row['status'] == 2) {
                    $this->error('已经提交审核');
                } else {
                    $row->save($data);
                    $this->success('提交审核成功');
                }
            }

            if ($type == 3) {
                $data['status2'] = $this->auth->id;
                if ($row['status2'] != '') {
                    $this->error('该报告已经被领取');
                } else {
                    $row->save($data);
                    $this->success('报告领取成功');
                }
            }

            if ($type == 4) {
                $data['status'] = 1;
                $row->save($data);
                $this->success('退回成功');
            }

            if ($type == 5) {
                $data['status'] = 3;
                $row->save($data);
                $this->success('操作成功');
            }


            if ($type == 6) {
                $data['status3'] = $this->auth->id;
                if ($row['status3'] != '') {
                    $this->error('该报告已经被领取');
                } else {
                    $row->save($data);
                    Presentationcomment::create([
                        'presentation_id'=>$row['id']
                    ]);
                    $this->success('报告领取成功');
                }
            }

            if ($type == 7) {
                $data['status'] = 4;
                $row->save($data);
                $this->success('操作成功');
            }
        }
    }

    public function detail($ids = "")
    {
        $this->relationSearch = true;
        // $row = $this->model->get(['id' => $ids]);
        $row = $this->model
            ->with(['medicine', 'factory'])
            ->where(['presentation.id' => $ids])
            ->find();

        if (!$row)
            $this->error(__('No Results were found'));
        $this->view->assign("row", $row->toArray());
        return $this->view->fetch();
    }


    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : true) : $this->modelValidate;
                        $this->model->validate($validate);
                    }
                    $result = $this->model->allowField(true)->save($params);
                    if ($result !== false) {
                        $this->success();
                    } else {
                        $this->error($this->model->getError());
                    }
                } catch (\think\exception\PDOException $e) {
                    $this->error($e->getMessage());
                } catch (\think\Exception $e) {
                    $this->error($e->getMessage());
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function create()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : true) : $this->modelValidate;
                        $this->model->validate($validate);
                    }
                    $result = $this->model->allowField(true)->save($params);
                    if ($result !== false) {
                        $this->success();
                    } else {
                        $this->error($this->model->getError());
                    }
                } catch (\think\exception\PDOException $e) {
                    $this->error($e->getMessage());
                } catch (\think\Exception $e) {
                    $this->error($e->getMessage());
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $ids = input('ids', 0, 'intval');
        $row = $this->model->find($ids);
        $this->assign('ids', $ids);
        $this->assign('row', $row);
        return $this->view->fetch();
    }


    /**
     * 编辑
     */
    public function view($ids = NULL)
    {
        $row = $this->model->get($ids);
        if (!$row)
            $this->error(__('No Results were found'));
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }

        $this->view->assign("row", $row);
        return $this->view->fetch();
    }
}
