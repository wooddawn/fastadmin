<?php

namespace app\admin\controller;

use app\common\controller\Backend;

/**
 * 医学评价
 *
 * @icon fa fa-circle-o
 */
class Presentationcomment extends Backend
{

    /**
     * Presentationcomment模型对象
     * @var \app\admin\model\Presentationcomment
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Presentationcomment;
        $this->view->assign("isknowList", $this->model->getIsknowList());
        $this->view->assign("activeList", $this->model->getActiveList());
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */


    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->with(['presentation'])
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->with(['presentation'])
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            foreach ($list as $row) {

                $row->getRelation('presentation')->visible(['name']);
            }
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }


    /**
     * 编辑
     */
    public function edit($ids = NULL)
    {
        $pid = intval(input('pids'));
        $where = $pid ? ['presentation_id' => $pid] : $ids;
        $row = $this->model->get($where);
        if (!$row)
            $this->error(__('No Results were found'));
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $json_content = json_decode($row['content'], 1);

        $row['contentcomment'] = (isset($json_content['contentcomment']) && $json_content['contentcomment']) ? $json_content['contentcomment'] : '{"中文":"","英文":""}';
        $row['contentrelation'] = (isset($json_content['contentrelation']) && $json_content['contentrelation']) ? $json_content['contentrelation'] : '{"中文":"","英文":""}';
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $array = [];
            if ($params['contentcomment']) foreach ($params['contentcomment'] as $key=> $v) {
                $array[$v['key']] = $v['value'];
            }
            $data['content']['contentcomment'] = json_encode($array,256);

            $array = [];
            if ($params['contentrelation']) foreach ($params['contentrelation'] as $key=> $v) {
                $array[$v['key']] = $v['value'];
            }
            $data['content']['contentrelation'] = json_encode($array,256);

            if ($params) {
                try {
                    $params['content'] = json_encode($data['content']);
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = basename(str_replace('\\', '/', get_class($this->model)));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : true) : $this->modelValidate;
                        $row->validate($validate);
                    }
                    $result = $row->allowField(true)->save($params);
                    if ($result !== false) {
                        $this->success();
                    } else {
                        $this->error($row->getError());
                    }
                } catch (\think\exception\PDOException $e) {
                    $this->error($e->getMessage());
                } catch (\think\Exception $e) {
                    $this->error($e->getMessage());
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }
}
