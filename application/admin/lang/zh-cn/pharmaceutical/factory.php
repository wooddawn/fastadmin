<?php

return [
    'Name'  =>  '药厂名',
    'Admin'  =>  '管理员',
    'Status'  =>  '状态',
    'Status 0'  =>  '隐藏',
    'Status 1'  =>  '正常',
    'Createtime'  =>  '创建时间',
    'Updatetime'  =>  '更新时间',
    'Admin.username'  =>  '管理员'
];
