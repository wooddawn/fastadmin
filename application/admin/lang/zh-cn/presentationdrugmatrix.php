<?php

return [
    'Presentation_drug_id'  =>  '药物信息',
    'Reactions_events_assessed'  =>  '评估的反应/事件',
    'Administration_and_start_of_reaction_event_number'  =>  '开始给药至反应/事件开始之间的时间间隔（数）',
    'Administration_and_start_of_reaction_event_unit'  =>  '开始给药至反应/事件开始之间的时间间隔（单位）',
    'Reaction_event_number'  =>  '末次给药至反应/事件开始之间的时间间隔（数）',
    'Reaction_event_unit'  =>  '末次给药至反应/事件开始之间的时间间隔（单位）',
    'Did_reaction_recur_on_re_administration'  =>  '重新给药后反应是否重复发生',
    'Did_reaction_recur_on_re_administration 1 '  =>  '是-是（已重新给药已重新',
    'Did_reaction_recur_on_re_administration 2 '  =>  '是-否（已重新给药已',
    'Did_reaction_recur_on_re_administration 3 '  =>  '是- 未知（已重新给药已',
    'Did_reaction_recur_on_re_administration 4 '  =>  '否-不适用（未重新给药',
    'Drug.characterisation_of_drug_role'  =>  '药物特征',
    'Drug.characterisation_of_drug_role 1 '  =>  '疑似药物疑似',
    'Drug.characterisation_of_drug_role 2 '  =>  '合并用药',
    'Drug.characterisation_of_drug_role 3 '  =>  '相互作用',
    'Drug.characterisation_of_drug_role 4 '  =>  '未给药'
];
