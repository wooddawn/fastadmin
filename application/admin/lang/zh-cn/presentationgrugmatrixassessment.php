<?php

return [
    'Presentation_drug_matrix_id'  =>  '药物信息',
    'Source_of_assessment'  =>  '评估来源',
    'Method_of_assessment'  =>  '评估方法',
    'Result_of_assessmentg'  =>  '评估结果',
    'Matrix.reactions_events_assessed'  =>  '评估的反应/事件'
];
