<?php

return [
    'Presentation_patient_char_parent_conditions_id'  =>  '报告',
    'Meddra_version_for_medical_history'  =>  '用于编码病史的MedDRA版本',
    'Medical_history_disease_surgical_procedure_etc_meddra_code'  =>  '病史（疾病/外科手术/等）（MedDRA编码）',
    'Start_date'  =>  '开始日期',
    'Continuing'  =>  '持续时间',
    'End_date'  =>  '结束日期',
    'Commentsd'  =>  '评论',
    'Conditions.presentation_patient_char_id'  =>  '报告',
    'Conditions.conditions_of_parentd'  =>  '相关病史及并发疾病的文本说明'
];
