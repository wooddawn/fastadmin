<?php

return [
    'Fa_presentation_id'  =>  '报告',
    'Title'  =>  '职称',
    'Givenname'  =>  '名',
    'Middlename'  =>  ' 中间名',
    'Familyname'  =>  '姓',
    'Organisation'  =>  '机构',
    'Department'  =>  '部门',
    'Address'  =>  '街道地址',
    'City'  =>  '城市',
    'Province'  =>  '州或省',
    'Postcode'  =>  '邮政编码',
    'Telephone'  =>  '电话号码',
    'Countycode'  =>  '国家代码',
    'Qualification'  =>  '资格',
    'Qualification 1'  =>  '医生',
    'Qualification 2'  =>  '药剂师',
    'Qualification 3'  =>  '其他医疗保健专业人士',
    'Qualification 4'  =>  '律师',
    'Qualification 5'  =>  '消费者或其他非医疗保健专业人士',
    'Type'  =>  '信息来源',
    'Type 1'  =>  '制药公司',
    'Type 2'  =>  '监管机构',
    'Type 3'  =>  '医疗保健专业人士',
    'Type 4'  =>  '地区药物警戒中心',
    'Type 5'  =>  'WHO国际药物监测合作中心',
    'Type 6 '  =>  '其他（例如：经销商或其他机构）',
    'Type 7'  =>  '患者/消费者'
];
