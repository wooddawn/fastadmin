<?php

return [
    'Presentation_patient_char_id'  =>  '报告',
    'Parent_identification'  =>  '父母识别',
    'Date_of_birth_of_parent'  =>  '父母的出生日期',
    'Age_of_parent'  =>  '父母的年龄',
    'Age_of_parent_number'  =>  '父母的年龄（数）',
    'Age_of_parent_unit'  =>  '父母的年龄（单位）',
    'Last_menstrual_period_date'  =>  '母亲末次月经日期',
    'Body_weight_kg_of_parent'  =>  '父母的体重（kg）',
    'Height_cm_of_parent'  =>  '父母的身高（cm）',
    'Sex_of_parentd'  =>  '父母的性别',
    'Sex_of_parentd 1'  =>  '男性',
    'Sex_of_parentd 2'  =>  '女性'
];
