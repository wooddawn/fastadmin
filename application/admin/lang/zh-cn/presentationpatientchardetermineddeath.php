<?php

return [
    'Presentation_patient_char_id'  =>  '报告',
    'Meddra_version_for_autopsy_determined_causes_of_death'  =>  '用于尸检确定的死因的MedDRA版本',
    'Autopsy-determined_causes_of_death_meddra_code'  =>  '尸检-确定的死因（MedDRA编码）',
    'Autopsy-determined_causes_of_death_free_text'  =>  '尸检-确定的死因（自定义文本）',
    'Char.patient_name_or_initials'  =>  '患者（姓名或姓名首字母缩写）'
];
