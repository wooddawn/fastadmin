<?php

return [
    'Presentation_id'  =>  '报告',
    'Characterisation_of_drug_role'  =>  '药物特征',
    'Mpid_version_date_number'  =>  ' MPID版本日期/编号',
    'Medicinal_product_identifier_mpid'  =>  '药品标识符（MPID）',
    'Phpid_version_date_number'  =>  ' PhPID版本日期/编号',
    'Pharmaceutical_product_identifier_phpid'  =>  'G.k.2.1.2b –药物制剂标识符（PhPID）',
    'Medicinal_product_name_as_reported_by_the_primary_source'  =>  '主要来源报告的药品名称',
    'Identification_of_the_country_where_the_drug_was_obtained'  =>  'G.k.2.4 –药品获得国的确认–药品获得国的确认',
    'Investigational_product_blinded'  =>  '设盲的试验用产品',
    'Authorisation_application_number'  =>  'G.k.3.1 –上市许可–上市许可/申请编号',
    'Country_of_authorisation_application'  =>  'G.k.3.2 –上市许可–上市许可/申请国家',
    'Name_of_holder_applicant'  =>  '持有人/申请人姓名',
    'Cumulative_dose_to_first_reaction_number'  =>  '首次发生反应的累积剂量（数值数值）',
    'Cumulative_dose_to_first_reaction_unit'  =>  '首次发生反应的累积剂量（单位）',
    'Gestation_period_at_time_of_exposure_number'  =>  '暴露时的妊娠期（数值数值）',
    'Gestation_period_at_time_of_exposure_unit'  =>  '暴露时的妊娠期（单位）',
    'Actions_taken_with_drug'  =>  '针对药物采取的措施',
    'Additional_information_on_drug_free_text'  =>  '药物的附加信息（自定义文本）',
    'Presentation.name'  =>  '患者姓名'
];
