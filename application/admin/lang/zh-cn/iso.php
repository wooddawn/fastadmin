<?php

return [
    'Number'  =>  'IOS版本号',
    'Name_cn'  =>  '中文名称',
    'Code'  =>  '编码',
    'Name_en'  =>  '英文名称',
    'Remark'  =>  '备注'
];
