<?php

return [
    'Presentation_id'  =>  '报告',
    'Content'  =>  '内容',
    'Isknow'  =>  '是否已知',
    'Isknow 1'  =>  '已知',
    'Isknow 0'  =>  '未知',
    'Active'  =>  '严重性',
    'Active 1'  =>  '严重',
    'Active 2'  =>  '一般',
    'Presentation.name'  =>  '患者姓名'
];
