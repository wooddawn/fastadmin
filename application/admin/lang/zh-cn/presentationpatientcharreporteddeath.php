<?php

return [
    'Presentation_patient_char_id'  =>  '报告',
    'Meddra_version_for_reported_causes_of_death'  =>  '用于报告死因的MedDRA版本',
    'Reported_causes_of_death_meddra_code'  =>  '报告的死因（MedDRA编码）',
    'Reported_causes_of_death_free_text'  =>  '报告的死因（自定义文本）',
    'Char.patient_name_or_initials'  =>  '患者（姓名或姓名首字母缩写）'
];
