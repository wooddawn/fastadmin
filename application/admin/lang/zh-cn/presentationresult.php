<?php

return [
    'Presentation_id'  =>  '报告',
    'Test_date'  =>  '检测日期–检测',
    'Test_name'  =>  '检测名称',
    'Meddra_version'  =>  'MedDRA版本',
    'Meddra_code'  =>  'MedDRA编码',
    'Code'  =>  '检测结果',
    'Code 2'  =>  '阴性',
    'Code 3'  =>  '边界值值',
    'Code 4'  =>  '没有结论没有结论',
    'Value'  =>  '检测结果(值/限定符)',
    'Unit'  =>  '检测结果（单位）',
    'Result'  =>  '非结构化结果数据结果',
    'Normal_low_value'  =>  '正常低值',
    'Normal_high_value'  =>  '正常高值',
    'Comments'  =>  '评论',
    'More_information'  =>  '是否有更多信息',
    'More_information 1'  =>  '有',
    'More_information 0'  =>  '没有'
];
