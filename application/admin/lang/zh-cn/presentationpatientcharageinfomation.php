<?php

return [
    'Presentation_patient_char_id'  =>  '报告',
    'Date_of_birth'  =>  '出生日期',
    'Age_at_time_of_onset_of_reaction_event_number'  =>  '反应/事件发生时的年龄（数）',
    'Age_at_time_of_onset_of_reaction_event_unit'  =>  '反应/事件发生时的年龄（单位）',
    'Foetus_number'  =>  '当胎儿的反应当胎儿的反应/事件被观察到时的妊娠期（数）',
    'Foetus_unit'  =>  '当胎儿的反应当胎儿的反应/事件被观察到时的妊娠期（单位）',
    'Patient_age_group_as_per_reporter'  =>  '患者年龄段年龄段（按报告者）',
    'Patient_age_group_as_per_reporter 0'  =>  '胎儿',
    'Patient_age_group_as_per_reporter 1'  =>  '婴儿（早产和足月新生儿）',
    'Patient_age_group_as_per_reporter 2'  =>  '幼儿',
    'Patient_age_group_as_per_reporter 3'  =>  '儿童',
    'Patient_age_group_as_per_reporter 4'  =>  '少年',
    'Patient_age_group_as_per_reporter 5'  =>  '成年',
    'Patient_age_group_as_per_reporter 6'  =>  '老年',
    'Char.patient_name_or_initials'  =>  '患者（姓名或姓名首字母缩写）'
];
