<?php

return [
    'Presentation_patient_char_id'  =>  '报告',
    'Conditions_of_parentd'  =>  '相关病史及并发疾病的文本说明',
    'Char.patient_name_or_initials'  =>  '患者（姓名或姓名首字母缩写）'
];
