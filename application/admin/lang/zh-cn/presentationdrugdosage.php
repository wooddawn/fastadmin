<?php

return [
    'Presentation_drug_id'  =>  '药物信息',
    'Dose_number'  =>  '剂量（数）',
    'Dose_unit'  =>  '剂量（单位）',
    'Number_of_units_in_the_interval'  =>  '间隔单位数',
    'Definition_of_the_time_interval_unit'  =>  '时间间隔单位的定义',
    'Date_and_time_of_start_of_drug'  =>  '开始用药的日期和时间',
    'Date_and_time_of_last_administration'  =>  '末次给药的日期和时间',
    'Duration_of_drug_administration_number'  =>  '给药持续时间（数值数值）',
    'Duration_of_drug_administration_unit'  =>  '给药持续时间（单位）',
    'Batch_lot_number'  =>  '批次/批号',
    'Dosage_text'  =>  '剂量文本',
    'Pharmaceutical_dose_form_free_text'  =>  '药物剂量表（自定义文本）',
    'Pharmaceutical_dose_form_termid_version_date_number'  =>  '药物剂型术语ID版本日期/编号',
    'Pharmaceutical_dose_form_termid'  =>  '药物剂型术语ID',
    'Route_of_administration_free_text'  =>  '给药途径（自定义文本）',
    'Route_of_administration_termid_version_date_number'  =>  '给药途径术语ID版本日期/编号',
    'Route_of_administration_termid'  =>  '给药途径术语ID',
    'Parent_route_of_administration_free_text'  =>  '主要给药途径（自定义文本）',
    'Parent_route_of_administration_termid_version_date_number'  =>  '主要给药途径术语ID版本日期/编号',
    'Parent_route_of_administration_termidg'  =>  '主要给药途径术语ID',
    'Drug.characterisation_of_drug_role'  =>  '药物特征',
    'Drug.characterisation_of_drug_role 1 '  =>  '疑似药物疑似',
    'Drug.characterisation_of_drug_role 2 '  =>  '合并用药',
    'Drug.characterisation_of_drug_role 3 '  =>  '相互作用',
    'Drug.characterisation_of_drug_role 4 '  =>  '未给药'
];
