<?php

return [
    'Presentation_drug_id'  =>  '药物信息',
    'Indication_as_reported_by_the_primary_source'  =>  '主要来源报告的适应症',
    'Meddra_version_for_indication'  =>  '针对适应症的MedDRA版本',
    'Indication_meddra_code'  =>  '适应症（MedDRA编码）',
    'Drug.characterisation_of_drug_role'  =>  '药物特征',
    'Drug.characterisation_of_drug_role 1 '  =>  '疑似药物疑似',
    'Drug.characterisation_of_drug_role 2 '  =>  '合并用药',
    'Drug.characterisation_of_drug_role 3 '  =>  '相互作用',
    'Drug.characterisation_of_drug_role 4 '  =>  '未给药'
];
