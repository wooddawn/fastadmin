<?php

return [
    'Presentation_id'  =>  '报告',
    'Studyname'  =>  '研究名称',
    'Studynumber'  =>  '申办者研究编号',
    'Studytype'  =>  '该反应/事件的研究类型',
    'Studytype 1'  =>  '临床试验',
    'Studytype 2'  =>  '个例患者使用个例患者使用（例如“同情使用同情使用”或“指定患者用途”）',
    'Studytype 3 '  =>  '其他研究（例如药物流行病学、药物经济学、重点监测重点）'
];
