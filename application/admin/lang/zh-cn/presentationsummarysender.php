<?php

return [
    'Presentation_summary_id'  =>  '报告',
    'Senders_diagnosis_event_meddra_code'  =>  '用于编码发送者诊断–用于编码发送者诊断 综合征和（或）反应 事件重新分类的MedDRA版本',
    'Senders_diagnosis_repeat_as_necessary'  =>  '发送者诊断 综合征和（或）对不良反应 事件的重新分类（MedDRA编码）'
];
