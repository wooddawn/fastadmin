<?php

return [
    'Uuid'  =>  '唯一标识码',
    'Createdate'  =>  '创建日期',
    'Type'  =>  '报告类型',
    'Type 1'  =>  '自发性报告',
    'Type 2'  =>  '来自研究的报告来自研究的报告',
    'Type 3 '  =>  '其他',
    'Type 4'  =>  '发送者无法获知（不详）',
    'Fristdate'  =>  '首次从来源收到报告日期',
    'Newdate'  =>  '收到本报告最新信息的日期',
    'Has_file'  =>  '是否提供附加文件',
    'Has_file true'  =>  '是',
    'Has_file false'  =>  '否',
    'Is_quick'  =>  '是否满足加速报告的本地标准',
    'Worldwideuuid'  =>  '全球唯一病例识别码',
    'Frist_sender'  =>  '首个发送者',
    'Frist_sender 1'  =>  '主管机构',
    'Frist_sender 2'  =>  '其他',
    'Other_case'  =>  '其他病例标识码',
    'Is_report'  =>  '报告作废/修订',
    'Is_report 1'  =>  '无效',
    'Is_report 2'  =>  '修正',
    'Presentation_id'  =>  '关联报告'
];
