<?php

return [
    'Name_cn'  =>  '中国惯用名',
    'Name_en'  =>  '英文名',
    'Name_fn'  =>  '法文',
    'Alpha_2'  =>  '二位字母',
    'Alpha_3'  =>  '三位字母',
    'Numberic'  =>  '数字'
];
