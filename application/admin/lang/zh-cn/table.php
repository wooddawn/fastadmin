<?php

return [
    'Table'  =>  '表名',
    'Name'  =>  '字段名',
    'Type'  =>  '字段类型',
    'Limit'  =>  '字段长度',
    'Oid'  =>  '字段OID',
    'Ismust'  =>  '是否必填',
    'Ismust 0'  =>  '否',
    'Ismust 1'  =>  '是',
    'Remark'  =>  '字段注释',
    'Index'  =>  '字段索引'
];
