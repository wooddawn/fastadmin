<?php

return [
    'Presentation_drug_id'  =>  '药物信息',
    'Additional_information_on_drug_coded'  =>  '药物的其他信息（编码）',
    'Additional_information_on_drug_coded 1'  =>  '假药',
    'Additional_information_on_drug_coded 2'  =>  '药物过量药物过量',
    'Additional_information_on_drug_coded 3'  =>  '父亲服用的药物',
    'Additional_information_on_drug_coded 4'  =>  '服用超过有效期的药物过',
    'Additional_information_on_drug_coded 5'  =>  '经检测合格的批次和批号',
    'Additional_information_on_drug_coded 6'  =>  '经检测不合格的批次和批号',
    'Additional_information_on_drug_coded 7'  =>  '用药错误',
    'Additional_information_on_drug_coded 8'  =>  '误用',
    'Additional_information_on_drug_coded 9'  =>  '滥用',
    'Additional_information_on_drug_coded 10'  =>  '职业暴露',
    'Additional_information_on_drug_coded 11'  =>  '超说明书用药超说明书用药',
    'Drug.characterisation_of_drug_role'  =>  '药物特征',
    'Drug.characterisation_of_drug_role 1 '  =>  '疑似药物疑似',
    'Drug.characterisation_of_drug_role 2 '  =>  '合并用药',
    'Drug.characterisation_of_drug_role 3 '  =>  '相互作用',
    'Drug.characterisation_of_drug_role 4 '  =>  '未给药'
];
