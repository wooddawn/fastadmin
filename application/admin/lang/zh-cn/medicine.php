<?php

return [
    'Pharmaceutical_factory_id'  =>  '药厂',
    'Name'  =>  '药品名',
    'Type1'  =>  '产品剂型',
    'Usetype'  =>  '用法',
    'Leibie'  =>  '类别',
    'Cate'  =>  '药品分类',
    'Pingpai'  =>  '品牌',
    'Code'  =>  '批准文号',
    'Unit'  =>  '使用剂量',
    'Name_cn'  =>  '药品通用名',
    'Guige'  =>  '规格',
    'Date'  =>  '有效期',
    'Relation'  =>  '不良反应',
    'Createtime'  =>  '创建时间',
    'Updatetime'  =>  '更新时间',
    'Status'  =>  '状态',
    'Status 0'  =>  '未上市',
    'Status 1'  =>  '已上市',
    'Weigh'  =>  '排序',
    'Factory.name'  =>  '药厂名',
    'Factory.status'  =>  '状态',
    'Factory.status 0'  =>  '隐藏',
    'Factory.status 1'  =>  '正常',
    'Factory.createtime'  =>  '创建时间',
    'Factory.updatetime'  =>  '更新时间'
];
