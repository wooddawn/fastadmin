<?php

return [
    'Presentation_patient_char_id'  =>  '报告',
    'Meddra_version_for_medical_history'  =>  '用于病史的MedDRA版本',
    'Medical_history_disease_surgical_procedure_etc_meddra_code'  =>  '病史（疾病/外科手术/等）（MedDRA编码）',
    'Start_date'  =>  '开始日期',
    'Continuing'  =>  '持续时间',
    'End_date'  =>  '结束日期',
    'Comments_content'  =>  '评论',
    'Family_historyd'  =>  '家族史',
    'Char.patient_name_or_initials'  =>  '患者（姓名或姓名首字母缩写）'
];
