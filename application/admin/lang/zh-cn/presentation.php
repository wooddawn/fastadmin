<?php

return [
    'Pharmaceutical_factory_id'  =>  '药厂名',
    'Medicine_id'  =>  '药品名',
    'Argno'  =>  '报告编号',
    'Name'  =>  '患者姓名',
    'Event'  =>  '反应事件',
    'Market'  =>  '药品状态',
    'Market 0'  =>  '未上市',
    'Market 1'  =>  '已经上市',
    'Date'  =>  '报告日期',
    'Isindex'  =>  '是否首次',
    'Isindex 0'  =>  '首次',
    'Isindex 1'  =>  '随访',
    'Status'  =>  '报告状态',
    'Status 0'  =>  '待录入',
    'Status 1'  =>  '录入中',
    'Status 2'  =>  '审核中',
    'Status 3'  =>  '评价中',
    'Status 4'  =>  '评价复核',
    'Status 5'  =>  '报告生成中',
    'Status 6'  =>  '上传中',
    'Doctor'  =>  '报告医生',
    'Pdffiles'  =>  '报告文档',
    'Createtime'  =>  '创建时间',
    'Updatetime'  =>  '更新时间',
    'Medicine.name'  =>  '药品名',
    'Factory.name'  =>  '药厂名',
    'Save'  => '保存',
    'Status1'  =>  '报告录入',
    'Status2'  =>  '报告审核',
    'Status3'  =>  '医学评价',
    'Status4'  =>  '评价复核',
    'Status5'  =>  '生成报告',
    'Status6'  =>  '上传报告',

    'Check 0'  =>  '提交审核',
    'Check 1'  =>  '未通过 - 退回修改',
    'Check 2'  =>  '审核通过 - 转下一步', //to status3
    'Check 3'  =>  '提交评价', //to status 4
    'Check 4'  =>  '医评复核', //tostatus 3
    'Check 5'  =>  '生成报告',
    'Check 6'  =>  '上传报告',
    'View Presentation'  =>  '查看报告',
    'Get Presentation'  =>  '领取报告',
    'Presentation Create'  =>  '录入报告',
    'Create Comment'  =>  '添加医学评价',


];
