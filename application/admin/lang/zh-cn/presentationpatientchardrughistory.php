<?php

return [
    'Presentation_patient_char_id'  =>  '报告',
    'Name_of_drug_as_reported'  =>  '报告所用药物的名称',
    'Mpid_version_date_number'  =>  ' MPID版本日期/编号 ISO IDMP',
    'Medicinal_product_identifier_mpid'  =>  '药品标识符（MPID）',
    'Phpid_version_date_number'  =>  ' PhPID版本日期/编号',
    'Pharmaceutical_product_identifier_phpid'  =>  '药剂标识符（PhPID）',
    'Start_date'  =>  '开始日期',
    'End_date'  =>  '结束日期',
    'Meddra_version_for_indication'  =>  '针对适应症的MedDRA版本',
    'Indication_meddra_code'  =>  '适应症（MedDRA编码）',
    'Meddra_version_for_reaction'  =>  '针对反应的MedDRA版本',
    'Reaction_meddra_code'  =>  '反应（MedDRA编码）',
    'Char.patient_name_or_initials'  =>  '患者（姓名或姓名首字母缩写）'
];
