<?php

return [
    'Presentation_drug_id'  =>  '药物信息',
    'Substance_specified_substance_name'  =>  '物质/指定物质名称',
    'Substancespecified_substance_termid_version_date_number'  =>  '物质/指定物质TermID版本日期/编号',
    'Substancespecified_substance_termid'  =>  '物质/指定物质术语ID',
    'Strength_number'  =>  '规格（数值数值）',
    'Strength_unit'  =>  '规格（单位）',
    'Drug.characterisation_of_drug_role'  =>  '药物特征',
    'Drug.characterisation_of_drug_role 1 '  =>  '疑似药物疑似',
    'Drug.characterisation_of_drug_role 2 '  =>  '合并用药',
    'Drug.characterisation_of_drug_role 3 '  =>  '相互作用',
    'Drug.characterisation_of_drug_role 4 '  =>  '未给药'
];
