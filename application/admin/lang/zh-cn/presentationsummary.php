<?php

return [
    'Presentation_id'  =>  '报告',
    'Outcome_and_additional_relevant_information'  =>  '病例叙述包括临床病程、治疗措施、治疗结果及其他相关信息',
    'Reporters_comments'  =>  '报告者的评论',
    'Narrative_case_summary_and_other_information'  =>  '发送者的评论',
    'Case_summary_and_reporters_comments_text'  =>  '病例总结和报告者评论文本',
    'Case_summary_and_reporters_comments_languageh'  =>  '病例总结和报告者评论语言',
    'Senders_diagnosis_event_meddra_code'  =>  '用于编码发送者诊断–用于编码发送者诊断 综合征和（或）反应 事件重新分类的MedDRA版本',
    'Senders_diagnosis_repeat_as_necessary'  =>  '发送者诊断 综合征和（或）对不良反应 事件的重新分类（MedDRA编码）'
];
