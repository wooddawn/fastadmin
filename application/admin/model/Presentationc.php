<?php

namespace app\admin\model;

use think\Model;

class Presentationc extends Model
{
    // 表名
    protected $name = 'presentationc';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'type_text',
        'has_file_text',
        'is_quick_text',
        'frist_sender_text',
        'other_case_text',
        'is_report_text'
    ];
    

    
    public function getTypeList()
    {
        return ['1' => __('Type 1'),'2' => __('Type 2'),'3 ' => __('Type 3 '),'4' => __('Type 4')];
    }     

    public function getHasFileList()
    {
        return ['true' => __('Has_file true'),'false' => __('Has_file false')];
    }     

    public function getIsQuickList()
    {
        return ['true' => __('True'),'false' => __('False'),'null' => __('Null')];
    }     

    public function getFristSenderList()
    {
        return ['1' => __('Frist_sender 1'),'2' => __('Frist_sender 2')];
    }     

    public function getOtherCaseList()
    {
        return ['true' => __('True'),'null' => __('Null')];
    }     

    public function getIsReportList()
    {
        return ['1' => __('Is_report 1'),'2' => __('Is_report 2')];
    }     


    public function getTypeTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getHasFileTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['has_file']) ? $data['has_file'] : '');
        $list = $this->getHasFileList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsQuickTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['is_quick']) ? $data['is_quick'] : '');
        $list = $this->getIsQuickList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getFristSenderTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['frist_sender']) ? $data['frist_sender'] : '');
        $list = $this->getFristSenderList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getOtherCaseTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['other_case']) ? $data['other_case'] : '');
        $list = $this->getOtherCaseList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsReportTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['is_report']) ? $data['is_report'] : '');
        $list = $this->getIsReportList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
