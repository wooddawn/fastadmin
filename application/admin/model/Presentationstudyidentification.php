<?php

namespace app\admin\model;

use think\Model;

class Presentationstudyidentification extends Model
{
    // 表名
    protected $name = 'presentationStudyIdentification';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'studytype_text'
    ];
    

    
    public function getStudytypeList()
    {
        return ['1' => __('Studytype 1'),'2' => __('Studytype 2'),'3 ' => __('Studytype 3 ')];
    }     


    public function getStudytypeTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['studytype']) ? $data['studytype'] : '');
        $list = $this->getStudytypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function presentation()
    {
        return $this->belongsTo('Presentation', 'presentation_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }



}
