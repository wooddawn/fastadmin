<?php

namespace app\admin\model;

use think\Model;

class Presentationevent extends Model
{
    // 表名
    protected $name = 'presentationEvent';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'highlighted_text',
        'death_text',
        'life_threatening_text',
        'hospitalisation_text',
        'incapacitating_text',
        'birth_defect_text',
        'other_condition_text',
        'outcome_text',
        'medical_confirmation_text'
    ];
    

    
    public function getHighlightedList()
    {
        return ['1' => __('Highlighted 1'),'2' => __('Highlighted 2'),'3' => __('Highlighted 3'),'4' => __('Highlighted 4')];
    }     

    public function getDeathList()
    {
        return ['1' => __('Death 1'),'0' => __('Death 0')];
    }     

    public function getLifeThreateningList()
    {
        return ['1' => __('Life_threatening 1'),'0' => __('Life_threatening 0')];
    }     

    public function getHospitalisationList()
    {
        return ['1' => __('Hospitalisation 1'),'0' => __('Hospitalisation 0')];
    }     

    public function getIncapacitatingList()
    {
        return ['1' => __('Incapacitating 1'),'0' => __('Incapacitating 0')];
    }     

    public function getBirthDefectList()
    {
        return ['1' => __('Birth_defect 1'),'0' => __('Birth_defect 0')];
    }     

    public function getOtherConditionList()
    {
        return ['1' => __('Other_condition 1'),'0' => __('Other_condition 0')];
    }     

    public function getOutcomeList()
    {
        return ['1' => __('Outcome 1'),'2' => __('Outcome 2'),'3' => __('Outcome 3'),'4' => __('Outcome 4'),'5' => __('Outcome 5'),'0' => __('Outcome 0')];
    }     

    public function getMedicalConfirmationList()
    {
        return ['1' => __('Medical_confirmation 1'),'0' => __('Medical_confirmation 0')];
    }     


    public function getHighlightedTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['highlighted']) ? $data['highlighted'] : '');
        $list = $this->getHighlightedList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getDeathTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['death']) ? $data['death'] : '');
        $list = $this->getDeathList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getLifeThreateningTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['life_threatening']) ? $data['life_threatening'] : '');
        $list = $this->getLifeThreateningList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getHospitalisationTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['hospitalisation']) ? $data['hospitalisation'] : '');
        $list = $this->getHospitalisationList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIncapacitatingTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['incapacitating']) ? $data['incapacitating'] : '');
        $list = $this->getIncapacitatingList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getBirthDefectTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['birth_defect']) ? $data['birth_defect'] : '');
        $list = $this->getBirthDefectList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getOtherConditionTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['other_condition']) ? $data['other_condition'] : '');
        $list = $this->getOtherConditionList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getOutcomeTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['outcome']) ? $data['outcome'] : '');
        $list = $this->getOutcomeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getMedicalConfirmationTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['medical_confirmation']) ? $data['medical_confirmation'] : '');
        $list = $this->getMedicalConfirmationList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function presentation()
    {
        return $this->belongsTo('Presentation', 'presentation_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }



}
