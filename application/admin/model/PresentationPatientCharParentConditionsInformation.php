<?php

namespace app\admin\model;

use think\Model;

class PresentationPatientCharParentConditionsInformation extends Model
{
    // 表名
    protected $name = 'presentation_patient_char_parent_conditions_information';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [

    ];
    

    







    public function conditions()
    {
        return $this->belongsTo('Conditions', 'presentation_patient_char_parent_conditions_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
