<?php

namespace app\admin\model;

use think\Model;

class Presentationdrugmatrix extends Model
{
    // 表名
    protected $name = 'presentation_drug_matrix';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'did_reaction_recur_on_re_administration'
    ];
    

    
    public function getDidReactionRecurOnRe_administrationList()
    {
        return ['1 ' => __('Did_reaction_recur_on_re_administration 1 '),'2 ' => __('Did_reaction_recur_on_re_administration 2 '),'3 ' => __('Did_reaction_recur_on_re_administration 3 '),'4 ' => __('Did_reaction_recur_on_re_administration 4 ')];
    }     


    public function getDidReactionRecurOnRe_administrationTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['did_reaction_recur_on_re_administration']) ? $data['did_reaction_recur_on_re_administration'] : '');
        $list = $this->getDidReactionRecurOnre_administrationList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function drug()
    {
        return $this->belongsTo('Presentationdrug', 'presentation_drug_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
