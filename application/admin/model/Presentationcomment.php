<?php

namespace app\admin\model;

use think\Model;

class Presentationcomment extends Model
{
    // 表名
    protected $name = 'presentation_comment';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    
    // 追加属性
    protected $append = [
        'isknow_text',
        'active_text'
    ];
    

    
    public function getIsknowList()
    {
        return ['1' => __('Isknow 1'),'0' => __('Isknow 0')];
    }     

    public function getActiveList()
    {
        return ['1' => __('Active 1'),'2' => __('Active 2')];
    }     


    public function getIsknowTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['isknow']) ? $data['isknow'] : '');
        $list = $this->getIsknowList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getActiveTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['active']) ? $data['active'] : '');
        $list = $this->getActiveList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function presentation()
    {
        return $this->belongsTo('Presentation', 'presentation_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
