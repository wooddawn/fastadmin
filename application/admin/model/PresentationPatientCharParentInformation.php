<?php

namespace app\admin\model;

use think\Model;

class PresentationPatientCharParentInformation extends Model
{
    // 表名
    protected $name = 'presentation_patient_char_parent_information';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;

    // 追加属性
    protected $append = [
        'sex_of_parentd_text'
    ];



    public function getSexOfParentdList()
    {
        return ['1' => __('Sex_of_parentd 1'),'2' => __('Sex_of_parentd 2')];
    }


    public function getSexOfParentdTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['sex_of_parentd']) ? $data['sex_of_parentd'] : '');
        $list = $this->getSexOfParentdList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function conditions()
    {
        return $this->belongsTo('Conditions', 'presentation_patient_char_parent_conditions_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
