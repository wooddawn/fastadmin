<?php

namespace app\admin\model;

use think\Model;

class Presentationdrugother extends Model
{
    // 表名
    protected $name = 'presentation_drug_other';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'additional_information_on_drug_coded_text'
    ];
    

    
    public function getAdditionalInformationOnDrugCodedList()
    {
        return ['1' => __('Additional_information_on_drug_coded 1'),'2' => __('Additional_information_on_drug_coded 2'),'3' => __('Additional_information_on_drug_coded 3'),'4' => __('Additional_information_on_drug_coded 4'),'5' => __('Additional_information_on_drug_coded 5'),'6' => __('Additional_information_on_drug_coded 6'),'7' => __('Additional_information_on_drug_coded 7'),'8' => __('Additional_information_on_drug_coded 8'),'9' => __('Additional_information_on_drug_coded 9'),'10' => __('Additional_information_on_drug_coded 10'),'11' => __('Additional_information_on_drug_coded 11')];
    }     


    public function getAdditionalInformationOnDrugCodedTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['additional_information_on_drug_coded']) ? $data['additional_information_on_drug_coded'] : '');
        $list = $this->getAdditionalInformationOnDrugCodedList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function drug()
    {
        return $this->belongsTo('Presentationdrug', 'presentation_drug_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
