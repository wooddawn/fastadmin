<?php

namespace app\admin\model;

use think\Model;

class PresentationPatientCharDrugHistory extends Model
{
    // 表名
    protected $name = 'presentation_patient_char_drug_history';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [

    ];
    

    







    public function char()
    {
        return $this->belongsTo('PresentationPatientChar', 'presentation_patient_char_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
