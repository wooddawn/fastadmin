<?php

namespace app\admin\model;

use think\Model;

class Table extends Model
{
    // 表名
    protected $name = 'table';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'ismust_text'
    ];
    

    
    public function getIsmustList()
    {
        return ['0' => __('Ismust 0'),'1' => __('Ismust 1')];
    }     


    public function getIsmustTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['ismust']) ? $data['ismust'] : '');
        $list = $this->getIsmustList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
