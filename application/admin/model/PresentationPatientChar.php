<?php

namespace app\admin\model;

use think\Model;

class PresentationPatientChar extends Model
{
    // 表名
    protected $name = 'presentation_patient_char';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'sex_text',
        'concomitant_therapies_text',
        'was_autopsy_done_text'
    ];
    

    
    public function getSexList()
    {
        return ['1' => __('Sex 1'),'2' => __('Sex 2')];
    }     

    public function getConcomitantTherapiesList()
    {
        return ['1' => __('Concomitant_therapies 1'),'0' => __('Concomitant_therapies 0')];
    }     

    public function getWasAutopsyDoneList()
    {
        return ['1' => __('Was_autopsy_done 1'),'0' => __('Was_autopsy_done 0')];
    }     


    public function getSexTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['sex']) ? $data['sex'] : '');
        $list = $this->getSexList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getConcomitantTherapiesTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['concomitant_therapies']) ? $data['concomitant_therapies'] : '');
        $list = $this->getConcomitantTherapiesList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getWasAutopsyDoneTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['was_autopsy_done']) ? $data['was_autopsy_done'] : '');
        $list = $this->getWasAutopsyDoneList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function presentation()
    {
        return $this->belongsTo('Presentation', 'presentation_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
