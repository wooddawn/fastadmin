<?php

namespace app\admin\model;

use think\Model;

class Presentationinformation extends Model
{
    // 表名
    protected $name = 'presentation_information';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'qualification_text',
        'type_text'
    ];
    

    
    public function getQualificationList()
    {
        return ['1' => __('Qualification 1'),'2' => __('Qualification 2'),'3' => __('Qualification 3'),'4' => __('Qualification 4'),'5' => __('Qualification 5')];
    }     

    public function getTypeList()
    {
        return ['1' => __('Type 1'),'2' => __('Type 2'),'3' => __('Type 3'),'4' => __('Type 4'),'5' => __('Type 5'),'6 ' => __('Type 6 '),'7' => __('Type 7')];
    }     


    public function getQualificationTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['qualification']) ? $data['qualification'] : '');
        $list = $this->getQualificationList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getTypeTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
