<?php

namespace app\admin\model;

use think\Model;

class Presentationresult extends Model
{
    // 表名
    protected $name = 'presentationResult';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'code_text',
        'more_information_text'
    ];
    

    
    public function getCodeList()
    {
        return ['2' => __('Code 2'),'3' => __('Code 3'),'4' => __('Code 4')];
    }     

    public function getMoreInformationList()
    {
        return ['1' => __('More_information 1'),'0' => __('More_information 0')];
    }     


    public function getCodeTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['code']) ? $data['code'] : '');
        $list = $this->getCodeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getMoreInformationTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['more_information']) ? $data['more_information'] : '');
        $list = $this->getMoreInformationList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function presentation()
    {
        return $this->belongsTo('Presentation', 'presentation_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

}
