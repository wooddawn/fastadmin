<?php

namespace app\admin\model;

use think\Model;
use app\admin\model\Presentationc;

class Presentation extends Model
{
    // 表名
    protected $name = 'presentation';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    
    // 追加属性
    protected $append = [
        'market_text',
        'isindex_text'
    ];

    protected static function init(){
        self::afterInsert(function ($row) {
            $pk = $row->getPk();
            $info = Presentationc::where($pk,$row[$pk])->find();
            if(!$info){
                Presentationc::create([
                    'presentation_id'=>$row[$pk]
                ]);
            }
            $info = Presentationinformation::where($pk,$row[$pk])->find();
            if(!$info){
                Presentationinformation::create([
                    'presentation_id'=>$row[$pk]
                ]);
            }

            $info = PresentationPatientChar::where($pk,$row[$pk])->find();
            if(!$info){
                PresentationPatientChar::create([
                    'presentation_id'=>$row[$pk]
                ]);
            }

            $info = Presentationdrug::where($pk,$row[$pk])->find();
            if(!$info){
                Presentationdrug::create([
                    'presentation_id'=>$row[$pk]
                ]);
            }
            $info = Presentationsummary::where($pk,$row[$pk])->find();
            if(!$info){
                Presentationsummary::create([
                    'presentation_id'=>$row[$pk]
                ]);
            }


        });
    }
    
    public function getMarketList()
    {
        return ['0' => __('Market 0'),'1' => __('Market 1')];
    }     

    public function getIsindexList()
    {
        return ['0' => __('Isindex 0'),'1' => __('Isindex 1')];
    }

    public function getStatusList()
    {
        return [
            '0' => __('Status 0'),
            '1' => __('Status 1'),
            '2' => __('Status 2'),
            '3' => __('Status 3'),
            '4' => __('Status 4'),
            '5' => __('Status 5'),
            '6' => __('Status 6'),
        ];
    }


    public function getMarketTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['market']) ? $data['market'] : '');
        $list = $this->getMarketList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsindexTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['isindex']) ? $data['isindex'] : '');
        $list = $this->getIsindexList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusListTextArr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }



    public function medicine()
    {
        return $this->belongsTo('Medicine', 'medicine_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function factory()
    {
        return $this->belongsTo('app\admin\model\pharmaceutical\Factory', 'pharmaceutical_factory_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
