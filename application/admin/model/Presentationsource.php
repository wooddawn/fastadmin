<?php

namespace app\admin\model;

use think\Model;

class Presentationsource extends Model
{
    // 表名
    protected $name = 'presentation_source';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'qualification_text'
    ];
    

    
    public function getQualificationList()
    {
        return ['1' => __('Qualification 1'),'2' => __('Qualification 2'),'3' => __('Qualification 3'),'4' => __('Qualification 4'),'5' => __('Qualification 5')];
    }     


    public function getQualificationTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['qualification']) ? $data['qualification'] : '');
        $list = $this->getQualificationList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function presentation()
    {
        return $this->belongsTo('Presentation', 'presentation_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
