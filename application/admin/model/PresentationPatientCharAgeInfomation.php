<?php

namespace app\admin\model;

use think\Model;

class PresentationPatientCharAgeInfomation extends Model
{
    // 表名
    protected $name = 'presentation_patient_char_age_infomation';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'patient_age_group_as_per_reporter_text'
    ];
    

    
    public function getPatientAgeGroupAsPerReporterList()
    {
        return ['0' => __('Patient_age_group_as_per_reporter 0'),'1' => __('Patient_age_group_as_per_reporter 1'),'2' => __('Patient_age_group_as_per_reporter 2'),'3' => __('Patient_age_group_as_per_reporter 3'),'4' => __('Patient_age_group_as_per_reporter 4'),'5' => __('Patient_age_group_as_per_reporter 5'),'6' => __('Patient_age_group_as_per_reporter 6')];
    }     


    public function getPatientAgeGroupAsPerReporterTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['patient_age_group_as_per_reporter']) ? $data['patient_age_group_as_per_reporter'] : '');
        $list = $this->getPatientAgeGroupAsPerReporterList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function char()
    {
        return $this->belongsTo('PresentationPatientChar', 'presentation_patient_char_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
