<?php

namespace app\admin\model;

use think\Model;

class Presentationdrugmatrixassessment extends Model
{
    // 表名
    protected $name = 'presentation_drug_matrix_assessment';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [

    ];
    

    







    public function matrix()
    {
        return $this->belongsTo('Presentationdrugmatrix', 'presentation_drug_matrix_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
