<?php

namespace app\admin\model;

use think\Model;

class Presentationdrug extends Model
{
    // 表名
    protected $name = 'presentation_drug';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'characterisation_of_drug_role_text',
        'actions_taken_with_drug_text'
    ];
    

    
    public function getCharacterisationOfDrugRoleList()
    {
        return ['1 ' => __('Characterisation_of_drug_role 1 '),'2 ' => __('Characterisation_of_drug_role 2 '),'3 ' => __('Characterisation_of_drug_role 3 '),'4 ' => __('Characterisation_of_drug_role 4 ')];
    }     

    public function getActionsTakenWithDrugList()
    {
        return ['1' => __('Actions_taken_with_drug 1'),'2' => __('Actions_taken_with_drug 2'),'3' => __('Actions_taken_with_drug 3'),'4' => __('Actions_taken_with_drug 4'),'0' => __('Actions_taken_with_drug 0'),'9' => __('Actions_taken_with_drug 9')];
    }     


    public function getCharacterisationOfDrugRoleTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['characterisation_of_drug_role']) ? $data['characterisation_of_drug_role'] : '');
        $list = $this->getCharacterisationOfDrugRoleList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getActionsTakenWithDrugTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['actions_taken_with_drug']) ? $data['actions_taken_with_drug'] : '');
        $list = $this->getActionsTakenWithDrugList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function presentation()
    {
        return $this->belongsTo('Presentation', 'presentation_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
